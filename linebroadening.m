function data_conv = linebroadening(data,opt,sw)
% convolutes the input data with a Lorentian and/or a
% Gaussian line broadening by applying a fft, then convoluting with an
% exponential function and applying a fft again.
%
% input parameters: 
% data: data to be convoluted
% opt: options Map, should contain keys ('Lorentian'/'Gaussian' and
%       'lw_L'/'lw_G')
% sw: width of the data points
%
% output parameters:
% data_conv: convoluted data 
%
% April 2024 A. Kehl (akehl@gwdg.de)
%
    if opt("Lorentzian") == 1
        disp("1")
        % Lorentian 
        Deltaend_L = opt('lw_L')*pi/(sw); 
        endintens1 = ifft(data(:));
        for i = 1:size(data,2)
            endintens(i) = endintens1(i)*exp(-Deltaend_L*i);   % Lorentzian line shape
        end
        endintens(1)=0.37*endintens(1);
        endamp_2d_L_conv(:) = real(fft(endintens));
    
        if opt("Gaussian") == 1
            % Gaussian            
            lw = opt('lw_G');
            Deltaend_G=(lw*pi/(sw*sqrt(2*log(2))))^2;
            endintens1 = ifft(endamp_2d_L_conv(:)+1000);
            for i = 1:size(data,2)
                endintens(i) = endintens1(i)*exp(-Deltaend_G*i^2/2);   % Gaussian line shape
            end
            endintens(1)=0.5*endintens(1);
            data_conv(:) = -abs(fft(endintens));
            
        else 
            data_conv=endamp_2d_L_conv(:);
        end
    elseif opt("Gaussian")==1
        % Gaussian        
        lw = opt('lw_G');
        Deltaend_G=(lw*pi/(sw*sqrt(2*log(2))))^2;
        %disp(Deltaend_G)
        data=data+1000;
        endintens1 = ifft(data(:));
        for i = 1:length(data)%size(data,2)
            endintens(i) = endintens1(i)*exp(-Deltaend_G*i^2/2);   % Gaussian line shape
        end
        endintens(1)=0.5*endintens(1);
        data_conv(:) = -abs(fft(endintens(:)));
        
    else 
        data_conv=data;
    end
end