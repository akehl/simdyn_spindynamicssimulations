classdef CP
    % functions for the CP ENDOR experiment
    %
    % April 2024 A. Kehl (akehl@gwdg.de)
    %   
methods (Static=true)
    function expt = setFieldStrength(constants,expt,set,values)
        % set the mw and rf field strength (omega1e, etc.)
        % is directly called for the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % expt: the Map containing the experimental parameters
        % set: boolean, should the field strength be calculated from the
        %       pulse length
        % values: if not fixed (set=true), array with field strengths
        %
        % output parameters:
        % expt: updated Map with experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %   

        arguments
            constants
            expt
            set
            values=[]
        end

        % setting the field strength values as needed for the CP ENDOR experiment            
        t = expt("t");
        if set==false
            if size(values,2)==5
                expt("prep") = values(1)*2*pi*1e6;
                expt("SL") = values(2)*2*pi*1e6;
                expt("CP") = values(3)*2*pi*1e3;
                expt("oneE") = values(4)*2*pi*1e6;
                expt("oneN") = values(5)*2*pi*1e3;

                % omega1e in T for orientation preselection 
                expt("pulsewidth") = expt("prep")/(2*pi*constants("CONST1")*1e10);  
            elseif size(values,2)==3
                expt("prep") = values(1)*2*pi*1e6;
                expt("SL") = values(2)*2*pi*1e6;
                expt("CP") = values(3)*2*pi*1e3;
                expt("oneE") = 2*pi/(4*t(7));
                expt("oneN") = 2*pi/(2*t(5));    

                % omega1e in T for orientation preselection 
                expt("pulsewidth") = expt("prep")/(2*pi*constants("CONST1")*1e10);  
            elseif size(values,2)==2
                if t(1)==0
                    expt("prep") = 2*pi/(4*t(7));
                    expt("SL") = values(1)*2*pi*1e6;
                    expt("CP") = values(2)*2*pi*1e3;
                    % omega1e in T for orientation preselection 
                    expt("pulsewidth") = expt("SL")/(2*pi*constants("CONST1")*1e10); 
                else
                    expt("prep") = 2*pi/(4*t(1));
                    expt("SL") = values(1)*2*pi*1e6;
                    expt("CP") = values(2)*2*pi*1e3;
                    % omega1e in T for orientation preselection 
                    expt("pulsewidth") = expt("prep")/(2*pi*constants("CONST1")*1e10); 
                end
                expt("oneE") = 2*pi/(4*t(7));
                expt("oneN") = 2*pi/(2*t(5));    

 
            elseif values~=[]
                println("Wrong dimensions of input, should be: [omega_prep/2pi,omega_1e/2pi,omega_1n/2pi]");
            end
        else
            expt("prep") = 2*pi/(4*t(7));
            expt("SL") = 2*pi/(4*t(7));
            expt("CP") = 2*pi/(2*t(5));
            expt("oneE") = 2*pi/(4*t(7));
            expt("oneN") = 2*pi/(2*t(5));

            % omega1e in T for orientation preselection 
            expt("pulsewidth") = expt("oneE")/(2*pi*constants("CONST1")*1e10);
        end
    end

    function expt = defineCP(expIN,start_CP,Npts_CP,range_CP)
        % set the CP dimension
        % is directly called for the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % expIN: the Map containing the experimental parameters
        % start_CP: start frequency for CP pulse
        % Npts_CP: number of points for CP axis
        % range_CP: sweep range of CP axis
        %
        % output parameters:
        % expt: updated Map with experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  
        expt = expIN;
        expt("start_CP") = start_CP*1e6;
        expt("Npts_CP") = Npts_CP;
        expt("range_CP") = range_CP*1e6;
    end

    function [endor_amp,endor_amp_conv,x_coords,y_coords,v_L] = ENDOR(constants,spinSys,spinOps,expt,opt)
        % start CP ENDOR calculation
        % is directly called from the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % spinSys: the Map describing the spin system 
        % spinOps: the Map containing the spin operators
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        %
        % output parameters:
        % endor_amp: endor amplitude values
        % endor_amp_conv: endor values convoluted with lb
        % x_coords: coords of rf axis
        % y_coords: coords of cp axis
        % v_L: nuclear Larmor frequency
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  

        % prepare EPR and ENDOR calculations 
        paramsEPR   = prepareEPR(spinSys,expt);
        paramsENDOR = prepareENDOR(constants,spinSys,expt);
        
        x_coords = paramsENDOR("x_coords");
        x_coords = x_coords(:,1)';
        v_L = paramsENDOR("v_L");

        % for SC no 2D scan, and for Powder define y-axis
        if opt("powder")==false
            expt("Npts_CP")=1;
            expt("range_CP")=0;
        else
            if expt("Npts_CP")>1
                step_CP = expt("range_CP")/(expt("Npts_CP")-1);
                y_coords = zeros(expt("Npts_CP"));
                for ii = 1:expt("Npts_CP")
                    y_coords(ii) = expt("start_CP") + (ii-1)*step_CP;
                end
                y_coords = y_coords(:,1)';
            else
                % step_CP=0;
                y_coords =  expt("start_CP");
                y_coords = y_coords(:,1)';
            end
            paramsENDOR("y_coords")=y_coords;
        end
     
        % EPR calculation
        if opt('freqDomain')==false
            epr = calcOriFieldDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        else
            epr = calcOriFreqDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        end
            
        % ENDOR calculation
        if opt("Relax") == true
            endor_amp = CP.ENDORcalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation with relaxation
        else
            endor_amp = CP.ENDORcalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation
        end

        if opt("powder")==false
            y_coords=1;
        end

        % convolution with lb
        endor_amp_conv = linebroadening(endor_amp,opt,expt("range_EN"));
        
    end

    function endor_amp = ENDORcalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual CP ENDOR calculation (no relaxation)
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  

        % get values from Maps
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");
        t = expt("t");
        Nint=8;
        
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
             
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel = EPR("NQI_sel");

        CS_zz_sel = EPR("CS_zz_sel");
        D_zz_sel = EPR("D_zz_sel");

        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");
        Npts_CP = expt("Npts_CP");

        if expt("Npts_CP")>1
            step_CP = expt("range_CP")/(expt("Npts_CP")-1);
        else
            step_CP = 0;
        end

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        endor_amp = zeros(1,Npts_EN);
         length(B_sel)
        if length(B_sel)==0
            disp('no resonances found')
            return
        end

    
        % loop to repeat the calculation for every orientation 
        for j=1:length(B_sel)
            % set parameters for this orientation
            geff=geff_sel(j);
            B = B_sel(j);

            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);

            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);

            CS_zz = CS_zz_sel(j,:)*1e-12;
            D_zz = D_zz_sel(j,:);

     
            S = S_sel(j);
            offsets = offsets_sel(j,:);

            % % loop over nuclei if not all in one spinSys
            % for n = 1:nucs 

            % loop over all offsets (spin manifolds)
            for i = 1 : size(offsets,2) 
    
                v_off_S = offsets(i);
                off_1 = offsets(1);

                rho0 = getRho0(constants,paramsENDOR,B,geff,spinOps,spinSys,opt,HF_zz,HF_zy,HF_zx,NQI_zz);
            
                start_EN = paramsENDOR("start_EN");
                step_EN = paramsENDOR("step_EN");
    
                prep = expt("prep");
                sl = expt("SL");
                cp = expt("CP");
                oneE = expt("oneE");
                oneN = expt("oneN");

                % define free Hamiltonian
                Hfree_p = 2*pi*v_off_S*Sz;
                if spinSys('N_SpinSys')>1
                    if opt("powder")==1
                        mn = round(i/(2*I(1)+1));
                    else
                        mn = i;
                    end
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(mn)*Iz{1} + 2*pi*v_L(mn)*Iz{1}*CS_zz(mn)+ 2*pi*HF_zz(mn)*(Sz*Iz{1})+ 2*pi*HF_zy(mn)*(Sz*Iy{1})+ 2*pi*HF_zx(mn)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        Hfree_p = Hfree_p + NQI(mn,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(mn,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(mn,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,3,3)*Iz{1}*Iz{1};
                    else
                        Hfree_p = Hfree_p + pi*NQI_zz(mn)*(3*Iz{1} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end      

                    % get offset for sc CP calculation 
                    if opt("powder")==false
                        [v_CP,paramsENDOR] = CP.getOffset(opt,paramsENDOR,expt,spinSys,v_off_S,HF_zz,NQI_zz,mn,i);
                    end
                else
                    for mm=1:Ni_ENDOR                            
                        % HF
                        Hfree_p = Hfree_p - 2*pi*v_L(mm)*Iz{mm} + 2*pi*HF_zz(mm)*(Sz*Iz{mm}) + 2*pi*HF_zy(mm)*(Sz*Iy{mm})+ 2*pi*HF_zx(mm)*(Sz*Ix{mm})+2*pi*v_L(mm)*CS_zz(mm)*Iz{mm};
                        % NQI
                        if opt("Bterm")==true 
                            Hfree_p = Hfree_p + NQI(mm,1,1)*Ix{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,2)*Ix{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,3)*Ix{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,1)*Iy{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,2)*Iy{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,3)*Iy{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,1)*Iz{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,2)*Iz{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,3)*Iz{mm}*Iz{mm};
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 
                        end  

                        % get offset for sc CP calculation 
                        if opt("powder")==false
                            [v_CP,paramsENDOR] = CP.getOffset(opt,paramsENDOR,expt,spinSys,v_off_S,HF_zz,NQI_zz,mm,i);
                        end
                    end

                    if spinSys("D_used")==true
                        for mm = 2:(size(D_zz,2)+1)
                            dipC = 2*pi*D_zz(mm-1);
    
                            HD = zeros(size(Hfree_p));
    
                            HD = HD + Ix{1}*Ix{mm};
                            HD = HD + Ix{1}*Iy{mm};
                            HD = HD + Ix{1}*Iz{mm};
                            HD = HD + Iy{1}*Ix{mm};
                            HD = HD + Iy{1}*Iy{mm};
                            HD = HD + Iy{1}*Iz{mm};
                            HD = HD + Iz{1}*Ix{mm};
                            HD = HD + Iz{1}*Iy{mm};
                            HD = HD + Iz{1}*Iz{mm};
                            
                            HDip = dipC*(3*Iz{1}*Iz{mm}-HD)/2;
                            Hfree_p = Hfree_p + HDip;
                        end
                    end
                end

                    % Hamiltonian for mw pulses    
                    Hprep_p =  Hfree_p + prep*Sx;
                    Hnonsel_p = Hfree_p + oneE*Sx;

                    % Integration step for the Signal to account for oscillation
                    if v_off_S==0
                        t11 = 1/(off_1*Nint);
                    else
                        t11 = 1/(v_off_S*Nint);
                    end


                    % Calculate the propagators
                    U1_p = expm(-1i*Hprep_p*t(1));
                    U2_p = expm(-1i*Hfree_p*t(2));
                    % SL/CP step
                    U4_p = expm(-1i*Hfree_p*t(4));
                    % RF pulse        
                    U6_p = expm(-1i*Hfree_p*t(6));
                    U7_p = expm(-1i*Hnonsel_p*t(7));                  
                    U8_p = expm(-1i*Hfree_p*t(8));
                    U9_p = expm(-1i*Hnonsel_p*t(9));
                    U10_p = expm(-1i*Hfree_p*(t(8)+t(7)/2));
                    U11_p = expm(-1i*Hfree_p*t11); 
                   
                % loop over cp frequencies (y-axis)
                for c = 1:Npts_CP

                if opt("powder")==true
                    v_CP = expt("start_CP") + step_CP*(c-1);
                end


                % loop over rf frequencies (x-axis)  
                parfor a = 1:Npts_EN
                % for a=1   %%% for testing
    
                    % Radiofrequency
                    v_RF = (start_EN+step_EN*(a-1));   
    
                    % Calculate the Free evolution hamiltonian
                    % (not including the Bterm)  

                    Hcorr = zeros(size(Hfree_p)); 
                    % Hamiltonian for RF pulse (no HF enhancement)
                    HRF = Hfree_p;
                    HRFb = zeros(size(Hfree_p));

                    HSL = Hfree_p + sl*Sy;

                    [V,D] = eig(HSL);
                    if ~issorted(diag(D))
                      [d,inds] = sort(diag(D));
                      D = diag(d);
                      V = V(:, inds);
                    end
                    %eigenvalues are order from negative large to positive large!
                    W = V^(-1);


                    if opt("Bterm")==false
                        if spinSys('N_SpinSys')>1
                            m=1;
                            HRF = HRF + 2*pi*v_RF*Iz{m}+oneN*Ix{m};
                            HRFb = 2*pi*(v_RF-v_CP)*Iz{m};
                            HSL = HSL + 2*pi*v_CP*Iz{m}+cp*Ix{m};                            
                        else
                            for mm = 1:Ni_ENDOR
                                HRF = HRF+ 2*pi*v_RF*Iz{mm}+oneN*Ix{mm};
                                HRFb = 2*pi*(v_RF-v_CP)*Iz{mm};
                                HSL = HSL + 2*pi*v_CP*Iz{mm}+cp*Ix{mm};                         
                            end
                        end
                    end

                    if spinSys('N_SpinSys')>1
                        m=1;
                        Hcorr = Hcorr + 2*pi*v_CP*Iz{m};
                    else

                        for mm = 1:Ni_ENDOR
                            Hcorr = Hcorr + 2*pi*v_CP*Iz{mm};
                        end
                    end                    

                    if opt("Bterm")==false
                        U3 = expm(-1i*HSL*t(3));
                        U5 = expm(-1i*HRF*t(5));  
                        U5b = expm(-1i*HRFb*t(5));

                        U1 = U1_p*expm(-1i*Hcorr*t(1));
                        U2 = U2_p*expm(-1i*Hcorr*t(2));

                        U4 = U4_p*expm(-1i*Hcorr*t(4));

                        U6 = U6_p*expm(-1i*Hcorr*t(6));
                        U7 = U7_p*expm(-1i*Hcorr*t(7));
                        U8 = U8_p*expm(-1i*Hcorr*t(8));
                        U9 = U9_p*expm(-1i*Hcorr*t(9));    
                        U10 = U10_p*expm(-1i*Hcorr*(t(8)+t(7)/2));
                        U11 = U11_p*expm(-1i*Hcorr*t11);
                    else
                        Hfree = Hfree_p;
                        HSL_p = Hfree + sl*Sy;

                        U3 = CP.CPBterm(opt,expt,v_RF,HSL_p,Ix,t(3),Ni_ENDOR,N_spinSys);
                        U5 = RFBterm(opt,expt,v_RF,Hfree,Iy,t(5),Ni_ENDOR,N_spinSys);

                        U1 = U1_p;
                        U2 = U2_p;
                        
                        U4 = U4_p;

                        U6 = U6_p;
                        U7 = U7_p;
                        U8 = U8_p;
                        U9 = U9_p;
                        U10 = U10_p;
                        U11 = U11_p;
                    end
                            
  
                    % Evolve the densitymatrix                
                    rho = rho0;
                    rho = U1*rho*U1';
                    rho = U2*rho*U2'; 
                    if t(2)~=0
                        rho = diag(diag(rho));   
                    end

                    rho = U3*rho*U3'; 

                    rho_t = W*rho*W^(-1);
                    rho_t = diag(diag(rho_t));
                    rho = V*rho_t*V^(-1);                    

                    rho = U4*rho*U4';
                    rho = U5*rho*U5'; 

                    if opt("Bterm")==true
                        rho = diag(diag(rho));
                    else
                        rho = U5b*rho*U5b';
                    end

                    rho = U6*rho*U6';
                    rho = U7*rho*U7';
                    rho = U8*rho*U8';
                    rho = U9*rho*U9';
                    rho = U10*rho*U10';
    

                    value_Sy = 0;
                    for b=1:Nint*10
                       rho = U11*rho*U11';
                       value_Sy = value_Sy+real(trace(rho*Sy));
                    end
    
                    if spinSys('N_SpinSys')>1 
                        if opt('powder')==0
                            s=1;
                        else
                            s=size(offsets,2)/Ni_ENDOR;
                        end
                    else
                        s=size(offsets,2);
                    end

                    endor_amp(a) = endor_amp(a)+(value_Sy*S/(Nint*s));

                end
                end
            end
            
        end
    end


    function endor_amp = ENDORcalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual CP ENDOR calculation (with relaxation)
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  

        spinOps_D = relaxation.diag_spinOps(spinOps,spinSys('Ni_ENDOR'));

        % get parameters from Maps
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");

        Sz_D = spinOps_D("Sz");
        Sx_D = spinOps_D("Sx");
        Sy_D = spinOps_D("Sy");
        Iz_D = spinOps_D("Iz");
        Ix_D = spinOps_D("Ix");
        Iy_D = spinOps_D("Iy");
        

        t = expt("t");
        Nint=8;
                
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
        Npts_CP = expt("Npts_CP");

        if expt("Npts_CP")>1
            step_CP = expt("range_CP")/(expt("Npts_CP")-1);
        else
            step_CP = 0;
        end
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
           
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel = EPR("NQI_sel");

        CS_zz_sel = EPR("CS_zz_sel");
        D_zz_sel = EPR("D_zz_sel");


        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        endor_amp = zeros(1,Npts_EN);
        % length(B_sel)

        if length(B_sel)==0
            disp('no resonances found')
            return
        end
    
        % loop to repeat the calculation for every orientation 
        for j=1:length(B_sel)
            % set parameters for this orientation
            geff=geff_sel(j);
            B = B_sel(j);

            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);

            CS_zz = CS_zz_sel(j,:)*1e-12;
            D_zz = D_zz_sel(j,:);


     
            S = S_sel(j);
            offsets = offsets_sel(j,:);
    
			% % loop over nuclei if not all in one spinSys
            % for n = 1 : nucs
            % loop over all offsets (spin manifolds)
            for i = 1 : size(offsets,2) 
    
                v_off_S = offsets(i);
                off_1 = offsets(1);

   
                rho0 = getRho0(constants,paramsENDOR,B,geff,spinOps,spinSys,opt,HF_zz,HF_zy,HF_zx,NQI_zz);

                RT1e = relaxation.RelaxT1(rho0,Sx_D,spinSys("T1e")); %electron T1
                RT1n = zeros(size(RT1e));
                RT2e = relaxation.RelaxT2(Sx_D,spinSys("T2e")); % electron T2
                RT2n = zeros(size(RT2e));

                if N_spinSys==1
                    for mm=1:Ni_ENDOR
                       RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{mm},spinSys("T1n")); %nuclear T1
                       RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{mm},spinSys("T2dq")); % double and zero quantum T2
                       RT2n = RT2n+relaxation.RelaxT2(Ix_D{mm},spinSys("T2n"));     % nuclear T2
                    end
                else
                    RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{1},spinSys("T1n")); %nuclear T1
                    RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{1},spinSys("T2dq")); % double and zero quantum T2
                    RT2n = RT2n+relaxation.RelaxT2(Ix_D{1},spinSys("T2n")); % nuclear T2
                end
                
                R = RT1e+RT1n+RT2e+RT2n; % full relaxation superoperator
                
                % for tilted frame Rho relaxation times
                RT1eRho = relaxation.RelaxT1(rho0,Sx_D,spinSys("T1eR")); %electron T1
                RT1nRho = zeros(size(RT1eRho));
                RT2eRho = relaxation.RelaxT2(Sx_D,spinSys("T2eR")); % electron T2
                RT2nRho = zeros(size(RT2eRho));

                if N_spinSys==1
                    for mm=1:Ni_ENDOR
                       RT1nRho = RT1eRho+relaxation.RelaxT1(rho0,Ix_D{mm},spinSys("T1nR")); %nuclear T1
                       RT2eRho = RT2eRho+relaxation.RelaxT2(Sx_D*Ix_D{mm},spinSys("T2dqR")); % double and zero quantum T2
                       RT2nRho = RT2nRho+relaxation.RelaxT2(Ix_D{mm},spinSys("T2nR"));     % nuclear T2
                    end
                else
                    RT1nRho = RT1eRho+relaxation.RelaxT1(rho0,Ix_D{1},spinSys("T1nR")); %nuclear T1
                    RT2eRho = RT2eRho+relaxation.RelaxT2(Sx_D*Ix_D{1},spinSys("T2dqR")); % double and zero quantum T2
                    RT2nRho = RT2nRho+relaxation.RelaxT2(Ix_D{1},spinSys("T2nR")); % nuclear T2
                end

                RRho = RT1eRho+RT1nRho+RT2eRho+RT2nRho; % full relaxation superoperator
            
                start_EN = paramsENDOR("start_EN");
                step_EN = paramsENDOR("step_EN");
    
                prep = expt("prep");
                sl = expt("SL");
                cp = expt("CP");
                oneE = expt("oneE");
                oneN = expt("oneN");

                Hfree_p = 2*pi*v_off_S*Sz;
                if spinSys('N_SpinSys')>1
                    if opt("powder")==1
                        mn = round(i/(2*I(1)+1));
                    else
                        mn = i;
                    end
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(mn)*Iz{1} + 2*pi*v_L(mn)*Iz{1}*CS_zz(mn)+ 2*pi*HF_zz(mn)*(Sz*Iz{1})+ 2*pi*HF_zy(mn)*(Sz*Iy{1})+ 2*pi*HF_zx(mn)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        Hfree_p = Hfree_p + NQI(mn,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(mn,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(mn,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(mn,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(mn,3,3)*Iz{1}*Iz{1};
                    else
                        Hfree_p = Hfree_p + pi*NQI_zz(mn)*(3*Iz{1} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end 

                    % get offset for sc CP calculation 
                    if opt("powder")==false
                        [v_CP,paramsENDOR] = CP.getOffset(opt,paramsENDOR,expt,spinSys,v_off_S,HF_zz,NQI_zz,mn,i);
                    end
                        
                else
                    for mm=1:Ni_ENDOR                            
                        % HF
                        Hfree_p = Hfree_p- 2*pi*v_L(mm)*Iz{mm} + 2*pi*HF_zz(mm)*(Sz*Iz{mm}) + 2*pi*HF_zy(mm)*(Sz*Iy{mm})+ 2*pi*HF_zx(mm)*(Sz*Ix{mm})+2*pi*v_L(mm)*CS_zz(mm)*Iz{mm};
                        % NQI
                        if opt("Bterm")==true 
                            Hfree_p = Hfree_p + NQI(mm,1,1)*Ix{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,2)*Ix{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,3)*Ix{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,1)*Iy{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,2)*Iy{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,3)*Iy{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,1)*Iz{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,2)*Iz{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,3)*Iz{mm}*Iz{mm};
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 
                        end    

                        % get offset for sc CP calculation 
                        if opt("powder")==false
                            [v_CP,paramsENDOR] = CP.getOffset(opt,paramsENDOR,expt,spinSys,v_off_S,HF_zz,NQI_zz,mm,i);
                        end
                    end
                     if spinSys("D_used")==true
                        for mm = 2:(size(D_zz,2)+1)
                            dipC = 2*pi*D_zz(mm-1);
    
                            HD = zeros(size(Hfree_p));
    
                            HD = HD + Ix{1}*Ix{mm};
                            HD = HD + Ix{1}*Iy{mm};
                            HD = HD + Ix{1}*Iz{mm};
                            HD = HD + Iy{1}*Ix{mm};
                            HD = HD + Iy{1}*Iy{mm};
                            HD = HD + Iy{1}*Iz{mm};
                            HD = HD + Iz{1}*Ix{mm};
                            HD = HD + Iz{1}*Iy{mm};
                            HD = HD + Iz{1}*Iz{mm};
                            
                            HDip = dipC*(3*Iz{1}*Iz{mm}-HD)/2;
                            Hfree_p = Hfree_p + HDip;
                        end
                    end
                end

                % Integration step for the Signal to account for oscillation
                if v_off_S==0
                    t11 = 1/(off_1*Nint);
                else
                    t11 = 1/(v_off_S*Nint);
                end

                % loop over cp frequencies (y-axis)
                for c = 1:Npts_CP

                if opt("powder")==true
                    v_CP = expt("start_CP") + step_CP*(c-1);
                end
                    
                % loop over rf frequencies (x-axis)
                parfor a = 1:Npts_EN
                % for a=1   %%% for testing

                    if abs(HF_zz(1))>1/spinSys("T2e")*0.1

                    % Radiofrequency
                    v_RF = (start_EN+step_EN*(a-1));   
    
                    Hcorr = zeros(size(Hfree_p));
                    % Hamiltonian for RF pulse (no HF enhancement)
                    HRF = Hfree_p;
                    HRFb = zeros(size(Hfree_p));
                    HSL = Hfree_p + sl*Sy;
                    

                    if opt("Bterm")==false
                        if spinSys('N_SpinSys')>1
                            m=1;
                            HRF = HRF + 2*pi*v_RF*Iz{m}+oneN*Ix{m};
                            HRFb = 2*pi*(v_RF-v_CP)*Iz{m};
                            HSL = HSL + 2*pi*v_CP*Iz{m}+cp*Ix{m};    
                        else
                            for mm = 1:Ni_ENDOR
                                HRF = HRF+ 2*pi*v_RF*Iz{mm}+oneN*Ix{mm};
                                HRFb = 2*pi*(v_RF-v_CP)*Iz{mm};
                                HSL = HSL + 2*pi*v_CP*Iz{mm}+cp*Ix{mm};     
                            end
                        end
                    end

                    [V,D] = eig(HSL);
                    if ~issorted(diag(D))
                      [d,inds] = sort(diag(D));
                      D = diag(d);
                      V = V(:, inds);
                    end
                    %eigenvalues are order from negative large to positive large!
                    W = V^(-1);

                    HSL_t = W*HSL*W^(-1);


                    if spinSys('N_SpinSys')>1
                        m=1;
                        Hcorr = Hcorr + 2*pi*v_CP*Iz{m};
                    else

                        for mm = 1:Ni_ENDOR
                            Hcorr = Hcorr + 2*pi*v_CP*Iz{mm};
                        end
                    end


                    Hfree = Hfree_p + Hcorr;

                    Hnonsel = relaxation.Liouville_N2byN2(Hfree + oneE*Sx);
                    Hprep = relaxation.Liouville_N2byN2(Hfree + prep*Sx);

                    Hfree = relaxation.Liouville_N2byN2(Hfree);


                        if opt("Bterm")==false
                            U3 = expm((RRho-1i*relaxation.Liouville_N2byN2(HSL_t))*t(3)); 

                            U5 = expm((R-1i*relaxation.Liouville_N2byN2(HRF))*t(5));  
                            
                            U1 = expm((R-1i*(Hprep))*t(1));
                            U2 = expm((R-1i*(Hfree))*t(2));

                            U4 = expm((R-1i*(Hfree))*t(4));

                            U6 = expm((R-1i*(Hfree))*t(6));
                            U7 = expm((R-1i*(Hnonsel))*t(7));
                            U8 = expm((R-1i*(Hfree))*t(8));
                            U9 = expm((R-1i*(Hnonsel))*t(9));
                            U10 = expm((R-1i*(Hfree))*(t(8)+t(7)/2));
                            U11 = expm((R-1i*(Hfree))*t11);
                        else
                            U3 = CP.CPBterm_withRelax(opt,expt,v_CP,HSL_t,Ix,t(3),Ni_ENDOR,N_spinSys,RRho);
                            U5 = relaxation.Bterm(opt,expt,v_RF,Hfree_p,Iy,t(5),Ni_ENDOR,N_spinSys,R);

                            U1 = expm((R-1i*(Hprep))*t(1));
                            U2 = expm((R-1i*(Hfree))*t(2));

                            U4 = expm((R-1i*(Hfree))*t(4));

                            U6 = expm((R-1i*(Hfree))*t(6));
                            U7 = expm((R-1i*(Hnonsel))*t(7));
                            U8 = expm((R-1i*(Hfree))*t(8));
                            U9 = expm((R-1i*(Hnonsel))*t(9));
                            U10 = expm((R-1i*(Hfree))*(t(8)+t(7)/2));
                            U11 = expm((R-1i*(Hfree))*t11);

                        end
 
                    % Evolve the densitymatrix  
                    rho = relaxation.MatrixToLiouvilleBra(rho0)';
                    rho = U1*rho;
                    rho = U2*rho;           

                    rho_t = W* relaxation.LiouvilleKetToMatrix(rho)*W^(-1);
                    rho_t = relaxation.MatrixToLiouvilleBra(rho_t)';
                    rho_t = U3*rho_t;
                    rho = V* relaxation.LiouvilleKetToMatrix(rho_t)*V^(-1);
                    rho = relaxation.MatrixToLiouvilleBra(rho)';

                    rho = U4*rho;
                    rho = U5*rho;          
                    rho = U6*rho;
                    rho = U7*rho;
                    rho = U8*rho;
                    rho = U9*rho;
                    rho = U10*rho;

                    value_Sy = 0;
                    for b=1:Nint
                       rho = U11*rho;
                       rho_f = relaxation.LiouvilleKetToMatrix(rho);
                       value_Sy = value_Sy+real(trace(rho_f*Sy));
                    end
                        endor_amp(a) = endor_amp(a)+(value_Sy*S/(Nint*size(offsets,2)));
                    end
                end
                end
            
            end
        end
    end

    function U = CPBterm(opt,expt,v_CP,H_IN,Ix,t,Ni_ENDOR,N_spinSys)
        % calculate the propagator of the CP pulse with Bterm 
        % 
        % input parameters:
        % opt: the Map containing the optional paramters
        % expt: the Map containing the experimental parameters
        % v_CP: CP frequency 
        % H_IN: acting Hamiltonian without RF term
        % Ix: the Ix spin Operator dictionary
        % t: pulse length
        % Ni_ENDOR: number of ENDOR nuclei
        % N_spinSys: number of spin systems
        %
        % output parameters:
        % U: propagator for the pulse
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  
        t_stepCP = 1/(v_CP*opt("N_stepRF"));
        U_SL = eye(size(H_IN));
        H_SL = H_IN;

        for ll = 1:opt("N_stepRF")
            % build the Hamiltonian for the increment
            if N_spinSys==1
                for mm = 1:Ni_ENDOR
                    H_SL = H_SL+  2*expt("CP")*Ix{mm}*cos(2*pi*v_CP*t_stepCP*(ll-1));
                end
            else
                mm=1;
                H_SL = H_SL+  2*expt("CP")*Ix{mm}*cos(2*pi*v_RF_CP*t_stepCP*(ll-1));
            end
            % create propagator and multiply with previous one
            U_SL = expm(-1i*H_SL*t_stepCP)*U_SL;
        end
        % build full propagator
        U = (U_SL^(t*v_CP));        
    end

    function U = CPBterm_withRelax(opt,expt,v_CP,H_IN,Ix,t,Ni_ENDOR,N_spinSys,R)
        % calculate the propagator of the CP pulse with Bterm and
        % relaxation
        % 
        % input parameters:
        % opt: the Map containing the optional paramters
        % expt: the Map containing the experimental parameters
        % v_CP: CP frequency 
        % H_IN: acting Hamiltonian without RF term
        % Ix: the Ix spin Operator dictionary
        % t: pulse length
        % Ni_ENDOR: number of ENDOR nuclei
        % N_spinSys: number of spin systems
        % R: relaxation superoperator
        %
        % output parameters:
        % U: propagator for the pulse
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  
        t_stepCP = 1/(v_CP*opt("N_stepRF"));

        H_SL = H_IN;
        format = size(H_IN,1);
        U_SL = eye(format^2,format^2);

        for ll = 1:opt("N_stepRF")
            % build the Hamiltonian for the increment
            if N_spinSys==1
                for mm = 1:Ni_ENDOR
                    H_SL = H_SL+  2*expt("CP")*Ix{mm}*cos(2*pi*v_CP*t_stepCP*(ll-1));
                end
            else
                mm=1;
                H_SL = H_SL+  2*expt("CP")*Ix{mm}*cos(2*pi*v_RF_CP*t_stepCP*(ll-1));
            end
            % create propagator and multiply with previous one
            U_SL = expm(R-1i*relaxation.Liouville_N2byN2(H_SL)*t_stepCP)*U_SL;
        end
        % build full propagator
        U = (U_SL^(t*v_CP));        
    end

    function [v_CP,paramsENDOR] = getOffset(opt,paramsENDOR_IN,expt,spinSys,v_off_S,HF_zz,NQI_zz,m,kk)
        % calculate the cp offset to hit one cp transition for the sc
        % calculation
        % only for I=1/2 and I=1
        % 
        % input parameters:
        % opt: the Map containing the optional paramters
        % paramsENDOR_IN: the Map containing the ENDOR parameters
        % expt: the Map containing the experimental parameters
        % spinSys: the Map describing the spin system
        % v_off_S: electron offset freq
        % HF_zz: effective HF coupling value
        % NQI_zz: effective NQC value
        % m: number of the nucleus (loop iteration)
        % kk: number of the offset (loop iteration)
        %
        % output parameters:
        % v_CP: cp frequency
        % paramsENDOR: updated Map
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  
        paramsENDOR = paramsENDOR_IN;
        I = spinSys("I");

        if opt("Bterm")==false
             if I(m)==1/2
                if kk<0

                    nu_alpha = sqrt((v_off_S+HF_zz(m)/2)^2+(expt("oneE")/(2*pi))^2);
                    nu_beta = sqrt((v_off_S-HF_zz(m)/2)^2+(expt("oneE")/(2*pi))^2);

                    if HF_zz(m)>0
                        offset_CP = 1/2*(nu_alpha+nu_beta);
                    else
                        offset_CP = 1/2*(nu_alpha-nu_beta);
                    end

                else

                    nu_alpha = sqrt((v_off_S+HF_zz(m)/2)^2+(expt("oneE")/(2*pi))^2);
                    nu_beta = sqrt((v_off_S-HF_zz(m)/2)^2+(expt("oneE")/(2*pi))^2);

                    if HF_zz(m)>0
                        offset_CP = 1/2*(nu_alpha-nu_beta);
                    else
                        offset_CP = -1/2*(nu_alpha+nu_beta);
                    end
                end

                if opt("sel_CP")==2
                    offset_CP = -offset_CP;
                end


            elseif I(m)==1
              % from diagonalization for 2D
                if kk==1
                    v_off_S = freq_EPR(2)-freq_EPR(3);
                elseif kk==0
                    v_off_S =0;
                elseif kk ==-1
                    v_off_S = freq_EPR(2)-freq_EPR(1);
                end


                omega_alpha = sqrt((v_off_S+HF_zz(m))^2+(expt("oneE")/(2*pi))^2);
                omega_beta = sqrt((v_off_S)^2+(expt("oneE")/(2*pi))^2);
                omega_gamma = sqrt((v_off_S-HF_zz(m))^2+(expt("oneE")/(2*pi))^2);


                if kk==1
                    if opt("sel_CP")==3
                        offset_CP = -(-omega_beta+omega_alpha+3*NQI_zz(m))/2;
                    elseif opt("sel_CP")==1
                        offset_CP =  -(omega_beta-omega_alpha+3*NQI_zz(m))/2;
                    end
                elseif kk==0
                    if opt("sel_CP")==4
                        offset_CP = (omega_gamma-omega_beta+3*NQI_zz(m))/2;                 
                    elseif opt("sel_CP")==3
                        offset_CP = -(-omega_beta-omega_alpha+3*NQI_zz(m))/2;                    
                    elseif opt("sel_CP")==2
                        offset_CP = (-omega_gamma+omega_beta+3*NQI_zz(m))/2;
                    elseif opt("sel_CP")==1                                      
                        offset_CP = -(+omega_beta+omega_alpha+3*NQI_zz(m))/2;   
                    end
                elseif kk==(-1)
                    if opt("sel_CP")==2
                        offset_CP = (omega_gamma+omega_beta+3*NQI_zz(m))/2;
                    elseif opt("sel_CP")==1
                        offset_CP = (-omega_gamma-omega_beta+3*NQI_zz(m))/2;
                    end
                end
            end
                    
        else 
            offset_CP=expt("start_CP");
        end
        v_L = paramsENDOR("v_L");
        v_CP = v_L(m)-offset_CP; 
        paramsENDOR("yCoords")=v_CP;
    end

    function [rho] = Step(constants,spinSys,spinOps,expt,opt)
        % start CP step calculation
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinSys: the Map describing the spin system 
        % spinOps: the Map containing the spin operators
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        %
        % output parameters:
        % rho: the final spin density matrix
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  			
        if opt("Relax")==0
            opt("Relax_step")=0;
            opt("Relax")=1;
            opt("temp_eff")=1;
        elseif ~isKey(opt,"Relax_step")
            opt("Relax_step")=1;
        end

        % prepare EPR and ENDOR calculations    
        paramsEPR   = prepareEPR(spinSys,expt);
        paramsENDOR = prepareENDOR(constants,spinSys,expt);


        % for SC no 2D scan, and for Powder define y-axis
        if opt("powder")==false
            expt("Npts_CP")=1;
            expt("range_CP")=0;
        else
            if expt("Npts_CP")>1
                step_CP = expt("range_CP")/(expt("Npts_CP")-1);
                y_coords = zeros(expt("Npts_CP"));
                for ii = 1:expt("Npts_CP")
                    y_coords(ii) = expt("start_CP") + (ii-1)*step_CP;
                end
                y_coords = y_coords(:,1)';
            else
                y_coords =  expt("start_CP");
                y_coords = y_coords(:,1)';
            end
            paramsENDOR("y_coords")=y_coords;
        end
      
        % EPR calculation
        if opt('freqDomain')==false
            epr = calcOriFieldDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        else
            epr = calcOriFreqDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        end
            
        % ENDOR calculation
        rho = CP.CPstep(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation

    end

    function rho = CPstep(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual CP step calculation 
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % rho: the final spin density matrix
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  			
        spinOps_D = relaxation.diag_spinOps(spinOps,spinSys('Ni_ENDOR'));

        % performs the actual calculation
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");

        Sz_D = spinOps_D("Sz");
        Sx_D = spinOps_D("Sx");
        Sy_D = spinOps_D("Sy");
        Iz_D = spinOps_D("Iz");
        Ix_D = spinOps_D("Ix");
        Iy_D = spinOps_D("Iy");

        t = expt("t");
        Nint=8;
        
        
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
        Npts_CP = expt("Npts_CP");

        if expt("Npts_CP")>1
            step_CP = expt("range_CP")/(expt("Npts_CP")-1);
        else
            step_CP = 0;
        end
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
            
        
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel = EPR("NQI_sel");

        CS_zz_sel = EPR("CS_zz_sel");
        
        offsets_sel = EPR("offsets");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        if length(B_sel)==0
            disp('no resonances found')
            return
        end
    
        % loop to repeat the calculation for every orientation 
        for j=1:length(B_sel)
    
            geff=geff_sel(j);
            B = B_sel(j);

            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);

            CS_zz = CS_zz_sel(j,:)*1e-12;

            offsets = offsets_sel(j,:);            

			% loop over nuclei if not all in one spinSys
            for n = 1 : nucs
            
                % loop over all offsets (spin manifolds)
                for i = 1 : size(offsets,2) 
        
                    v_off_S = offsets(i);
                    off_1 = offsets(1);
    
                    if opt("powder")==false
                        [v_CP,paramsENDOR] = CP.getOffset(opt,paramsENDOR,expt,spinSys,v_off_S,HF_zz,NQI_zz,n,i);
                    end
       
                    rho0 = getRho0(constants,paramsENDOR,B,geff,spinOps,spinSys,opt,HF_zz,HF_zy,HF_zx,NQI_zz);
    
                    RT1e = relaxation.RelaxT1(rho0,Sx_D,spinSys("T1e")); %electron T1
                    RT1n = zeros(size(RT1e));
                    RT2e = relaxation.RelaxT2(Sx_D,spinSys("T2e"));
                    RT2n = zeros(size(RT2e));
    
                    if N_spinSys==1
                        for mm=1:Ni_ENDOR
                           RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{mm},spinSys("T1n")); %nuclear T1
                           RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{mm},spinSys("T2dq"));
                           RT2n = RT2n+relaxation.RelaxT2(Ix_D{mm},spinSys("T2n"));
                        end
                    else
                        RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{1},spinSys("T1n"));
                        RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{1},spinSys("T2dq"));
                        RT2n = RT2n+relaxation.RelaxT2(Ix_D{1},spinSys("T2n"));
                    end
                    
                    R = RT1e+RT1n+RT2e+RT2n;
              
        
                    prep = expt("prep");
                    sl = expt("SL");
                    cp = expt("CP");
    
                    Hfree_p = 2*pi*v_off_S*Sz;
                    if spinSys('N_SpinSys')>1
                        nuc = round(i/(2*I(1)+1)) ;
                        mm =1;
                        % HF
                        Hfree_p = Hfree_p- 2*pi*v_L(mm)*Iz{mm} + 2*pi*v_L(mm)*Iz{mm}*CS_zz(nuc)+ 2*pi*HF_zz(nuc)*(Sz*Iz{mm})+ 2*pi*HF_zy(nuc)*(Sz*Iy{mm})+ 2*pi*HF_zx(nuc)*(Sz*Ix{mm});
                        % NQI
                        if opt("Bterm")==true 
                            Hfree_p = Hfree_p + NQI(mm,1,1)*Ix{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,2)*Ix{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,3)*Ix{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,1)*Iy{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,2)*Iy{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,3)*Iy{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,1)*Iz{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,2)*Iz{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,3)*Iz{mm}*Iz{mm};
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 
                        end            
                    else
                        for mm=1:Ni_ENDOR                               
                            % HF
                            Hfree_p = Hfree_p- 2*pi*v_L(mm)*Iz{mm} + 2*pi*HF_zz(mm)*(Sz*Iz{mm}) + 2*pi*HF_zy(mm)*(Sz*Iy{mm})+ 2*pi*HF_zx(mm)*(Sz*Ix{mm})+2*pi*v_L(mm)*CS_zz(mm)*Iz{mm};
                            % NQI
                            if opt("Bterm")==true 
                                Hfree_p = Hfree_p + NQI(mm,1,1)*Ix{mm}*Ix{mm};
                                Hfree_p = Hfree_p + NQI(mm,1,2)*Ix{mm}*Iy{mm};
                                Hfree_p = Hfree_p + NQI(mm,1,3)*Ix{mm}*Iz{mm};
                                Hfree_p = Hfree_p + NQI(mm,2,1)*Iy{mm}*Ix{mm};
                                Hfree_p = Hfree_p + NQI(mm,2,2)*Iy{mm}*Iy{mm};
                                Hfree_p = Hfree_p + NQI(mm,2,3)*Iy{mm}*Iz{mm};
                                Hfree_p = Hfree_p + NQI(mm,3,1)*Iz{mm}*Ix{mm};
                                Hfree_p = Hfree_p + NQI(mm,3,2)*Iz{mm}*Iy{mm};
                                Hfree_p = Hfree_p + NQI(mm,3,3)*Iz{mm}*Iz{mm};
                            else
                                Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 
                            end            
                        end
                    end
    
                    % Integration step for the Signal to account for oscillation
                    if v_off_S==0
                        t11 = 1/(off_1*Nint);
                    else
                        t11 = 1/(v_off_S*Nint);
                    end
    
                    % loop over all cp freq (y-axis)
                    for c = 1:Npts_CP
    
                        if opt("powder")==true
                            v_CP = expt("start_CP") + step_CP*(c-1);
                        end
           
                        Hcorr = zeros(size(Hfree_p));
                        HSL = Hfree_p + sl*Sy;
    
                        if spinSys('N_SpinSys')>1
                            m=1;
                            Hcorr = Hcorr + 2*pi*v_CP*Iz{m};
                        else
    
                            for mm = 1:Ni_ENDOR
                                Hcorr = Hcorr + 2*pi*v_CP*Iz{mm};
                            end
                        end
                        HSL_c = HSL + Hcorr;
                        [V,D] = eig(HSL_c);
                        if ~issorted(diag(D))
                          [d,inds] = sort(diag(D));
                          D = diag(d);
                          V = V(:, inds);
                        end
                        %eigenvalues are order from negative large to positive large!
                        W = V^(-1);  
    
    
                        if opt("Bterm")==false
                            if spinSys('N_SpinSys')>1
                                m=1;
                                HSL = HSL + 2*pi*v_CP*Iz{m}+cp*Ix{m};    
                            else
                                for mm = 1:Ni_ENDOR
                                    HSL = HSL + 2*pi*v_CP*Iz{mm}+cp*Ix{mm};     
                                end
                            end
                        end
    
    
    
                        Hfree = Hfree_p + Hcorr;
                        
                        Hprep = relaxation.Liouville_N2byN2(Hfree + prep*Sx);
    
                        Hfree = relaxation.Liouville_N2byN2(Hfree);
    
                            
                        HSL_t = W*HSL*W^(-1);
                        if opt("Bterm")==false
                            if opt("Relax_step")==0
                                U3 = expm((-1i*relaxation.Liouville_N2byN2(HSL_t))*t(3)); 
                                U1 = expm((-1i*(Hprep))*t(1));
                                U2 = expm((-1i*(Hfree))*t(2));
                            else
                                U3 = expm((R-1i*relaxation.Liouville_N2byN2(HSL_t))*t(3)); 
                                U1 = expm((R-1i*(Hprep))*t(1));
                                U2 = expm((R-1i*(Hfree))*t(2));
                            end
                        else
                            U3 = CP.CPBterm_withRelax(opt,expt,v_CP,HSL_t,Ix,t(3),Ni_ENDOR,N_spinSys,R);
                            U1 = expm((R-1i*(Hprep))*t(1));
                            U2 = expm((R-1i*(Hfree))*t(2));
                        end                       
                        
       
                        % Evolve the densitymatrix  
    
                        rho = relaxation.MatrixToLiouvilleBra(rho0)';
    
                        rho = U1*rho;
                        rho = U2*rho;
                        rho_t = relaxation.LiouvilleKetToMatrix(rho);
                        rho_t = W*rho_t*W^(-1);
                        rho = relaxation.MatrixToLiouvilleBra(rho_t)';
                        rho = U3*rho; 

                        rho = relaxation.LiouvilleKetToMatrix(rho);
    
                    end
                end
            end
        end
    end

end
end