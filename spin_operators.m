function Ops=spin_operators(S,I,N_Nuclei,N_SpinSys)
    % defines the spin operators 
    % 
    % input parameters:
    % S: electron spin quantum number
    % I: nuclear spin quantum numbers (array)
    % N_Nuclei: number nuclei
    % N_SpinSys: number spin systems
    %
    % output parameters:
    % Ops: the Map with the spin operators
    %
    % February 2024 A. Kehl (akehl@gwdg.de)

    Ops = containers.Map;
    %Define the Electron Spin operators in general

    if N_SpinSys>1
        N_Nuclei=1;
    end


    % Define the Electron Spin operators in general

    id_S = eye(2*S+1);
    Svalues = sqrt((1:2*S).*(2*S:-1:1));
    sigma_Sp = diag(Svalues,+1);
    sigma_Sm = diag(Svalues,-1);
    sigma_Sx = (sigma_Sp + sigma_Sm)/2;
    sigma_Sy = (sigma_Sp - sigma_Sm)/2i;
    sigma_Sz = diag(S:-1:-S);



    %Define the Nuclear Spin operators in general
    id_I{3} = [];

    for ii=1:N_Nuclei
        id_I{ii} = eye(2*I(ii)+1);
    
        Ivalues{ii} = sqrt((1:2*I(ii)).*(2*I(ii):-1:1));
        sigma_Ip{ii} = diag(Ivalues{ii},+1);
        sigma_Im{ii} = diag(Ivalues{ii},-1);
        sigma_Ix{ii} = (sigma_Ip{ii} + sigma_Im{ii})/2;
        sigma_Iy{ii} = (sigma_Ip{ii} - sigma_Im{ii})/2i;
        sigma_Iz{ii} = diag(I(ii):-1:-I(ii));
    end

    % Electron spin operators
    Sx = sigma_Sx;
    Sy = sigma_Sy;
    Sz = sigma_Sz;
    for ii=1:N_Nuclei
        Sx = kron(Sx, id_I{ii});
        Sy = kron(Sy, id_I{ii});
        Sz = kron(Sz, id_I{ii});        
    end

    Ops("Sx")=Sx;
    Ops("Sy")=Sy;
    Ops("Sz")=Sz;
    
    Ix{N_Nuclei} = [];
    Iy{N_Nuclei} = [];
    Iz{N_Nuclei} = [];
  
    for ii = 1:N_Nuclei
            Ix{ii} = kron(id_S, sigma_Ix{ii}); 
            Iy{ii}= kron(id_S, sigma_Iy{ii});
            Iz{ii}= kron(id_S, sigma_Iz{ii});

        for jj = 1:N_Nuclei       
          
            if jj>ii
                Ix{ii} = kron(Ix{ii},id_I{jj});
                Iy{ii} = kron(Iy{ii},id_I{jj});
                Iz{ii} = kron(Iz{ii},id_I{jj});
            elseif jj<ii
                Ix{ii} = kron(id_I{jj},Ix{ii});
                Iy{ii} = kron(id_I{jj},Iy{ii});
                Iz{ii} = kron(id_I{jj},Iz{ii});
            end
        end
     end
     
    Ops("Ix")=Ix;
    Ops("Iy")=Iy;
    Ops("Iz")=Iz;

end
