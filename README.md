# SimDyn_SpinDynamicsSimulations

Simulation routine for spin dynamics simulations (SimDyn) as described in:
A. Kehl 'Development of Analysis- and Simulation-Routines for ENDOR Spectroscopy', PhDThesis, Georg-August University, Göttingen, 2024.

## Installation/Download
The routine is running in MATLAB and were tested with MATLAB 2023b - older versions may also work though.
Download the files, and add them to the MATLAB folder structure.
Create input files and start simulations, examples are provided in a subfolder.

## Support
Annemarie Kehl akehl@gwdg.de

## Acknowledgment
Some sub-rotines were desined by co-workers or based on previous work and adapted for the framework. Detailed information on contributions are given in the Thesis mentioned above, here main contributors are listed (alphabetically):
I. Bejenke, M. Bennati, S.J. Glaser, F. Hecker, M. Hiller, I. Kaminker, R. Rizzato, I. Tkach, S. Vega, R. Zeier

