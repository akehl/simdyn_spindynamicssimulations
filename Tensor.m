classdef Tensor
    % functions to calculate the full tensor that could be detected without pulse effects
    %
    % February 2024 A. Kehl (akehl@gwdg.de)
    %   
methods (Static=true)
    function expt = setFieldStrength(constants,expt,set,values)
        % set the mw and rf field strength (omega1e, etc.)
        % is directly called for the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % expt: the Map containing the experimental parameters
        % set: boolean, should the field strength be calculated from the
        %       pulse length
        % values: if not fixed (set=true), array with field strengths
        %
        % output parameters:
        % expt: updated Map with experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %   
        arguments
            constants
            expt
            set
            values=[]
        end
        % setting the field strength values as needed for the full tensor
        % calculation

        if set==false
            if size(values,1)==2
                expt("oneE") = values(1)*2*pi*1e6;
                expt("oneN") = values(2)*2*pi*1e3;
        
                % omega1e in T for orientation preselection 
                expt("pulsewidth") = expt("oneE")/(2*pi*constants("CONST1")*1e10);        
            elseif values~=[]
                println("Wrong dimensions of input, should be: [omega_prep/2pi,omega_1e/2pi,omega_1n/2pi]");
            end
        else
            t = expt("t");
            expt("oneE") = 2*pi/(2*t(2));
            expt("oneN") = 2*pi/(2*t(1));

            % omega1e in T for orientation preselection 
            expt("pulsewidth") = expt("oneE")/(2*pi*constants("CONST1")*1e10);
        end
    end

    function [endor_amp,endor_amp_conv,x_coords,v_L] = fullTensor(constants,spinSys,spinOps,expt,opt)
        % start full tensor calculation
        % is directly called from the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % spinSys: the Map describing the spin system 
        % spinOps: the Map containing the spin operators
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        %
        % output parameters:
        % endor_amp: endor amplitude values
        % endor_amp_conv: endor values convoluted with lb
        % x_coords: coords of rf axis
        % v_L: nuclear Larmor frequency
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  

        % prepare EPR and ENDOR calculations 
        paramsEPR   = prepareEPR(spinSys,expt);
        paramsENDOR = prepareENDOR(constants,spinSys,expt);
        
        x_coords = paramsENDOR("x_coords");
        x_coords = x_coords(:,1)';
        v_L = paramsENDOR("v_L");
     
        % EPR calculation
        if opt('freqDomain')==false
            epr = calcOriFieldDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        else
            epr = calcOriFreqDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        end
            
        % ENDOR calculation
        if opt("Relax") == true
            disp('Only T2 Relaxation supported for the tesor calculation.');
            endor_amp = Tensor.fullTensorCalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation with relaxation
        else
            endor_amp = Tensor.fullTensorCalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation
        end

        % convolution with lb       
        endor_amp_conv = linebroadening(endor_amp,opt,expt("range_EN"));
    end

    function endor_amp = fullTensorCalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual ENDOR calculation (no relaxation)
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  		

        % redefine, one spin system is not possible
        spinSys('N_SpinSys') = spinSys("Ni_ENDOR");
        spinOps=spin_operators(spinSys("S"),spinSys("I"),spinSys("Ni_ENDOR"),spinSys("Ni_ENDOR"));

        % get values from Maps
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");
        t = expt("t");
        Nint=8;
        
        N_spinSys = spinSys("Ni_ENDOR");
        Ni_ENDOR = spinSys("Ni_ENDOR");
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
            
    
        
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel = EPR("NQI_sel");
        CS_zz_sel = EPR("CS_zz_sel");
        D_zz_sel = EPR("D_zz_sel");

        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");



    
        endor_amp = zeros(1,Npts_EN);

        if length(B_sel)==0
            disp('no resonances found')
            return
        end

        % loop to repeat the calculation for every orientation 
        parfor j=1:length(B_sel)
            % set parameters for this orientation
            geff=geff_sel(j);
            B = B_sel(j);
            const_R = 1/size(Sz,2)*constants('GE')*B/(2*pi*constants('K_B')*opt("T"));

            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);
            
            CS_zz = CS_zz_sel(j,:)*1e-12;
            D_zz = D_zz_sel(j,:);

     
            S = S_sel(j);
            offsets = offsets_sel(j,:);

            endor_amp_tmp = zeros(1,Npts_EN);

            % loop over nuclei 
            for i =1 : Ni_ENDOR
                I = spinSys('I');
                mI = -I(i):1:I(i);

                for jj = mI
    
                    v_off_S = jj*HF_zz(i);
                    off_1 = -I(i)*HF_zz(i);
       
                    [rho0] = 2*Sz*Iz{i};   

                    start_EN = paramsENDOR("start_EN");
                    step_EN = paramsENDOR("step_EN");
        
                    oneE = expt("oneE");
                    oneN = expt("oneN");
    
                    Hfree_p = 2*pi*v_off_S*Sz;
                    nuc = i                   
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(nuc)*Iz{1} + 2*pi*v_L(nuc)*Iz{1}*CS_zz(nuc)+ 2*pi*HF_zz(nuc)*(Sz*Iz{1})+ 2*pi*HF_zy(nuc)*(Sz*Iy{1})+ 2*pi*HF_zx(nuc)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        m =1;
                        Hfree_p = Hfree_p + NQI(m,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(m,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(m,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,3,3)*Iz{1}*Iz{1};
                    else
                        m =1;
                        Hfree_p = Hfree_p + pi*NQI_zz(m)*(3*Iz{m} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end 

                    if spinSys("D_used")==true && i==1
                        for mm = 2:(size(D_zz,2)+1)
                            dipC = 2*pi*D_zz(mm-1);
    
                            HD = zeros(size(Hfree_p));
    
                            HD = HD + Ix{1}*Ix{mm};
                            HD = HD + Ix{1}*Iy{mm};
                            HD = HD + Ix{1}*Iz{mm};
                            HD = HD + Iy{1}*Ix{mm};
                            HD = HD + Iy{1}*Iy{mm};
                            HD = HD + Iy{1}*Iz{mm};
                            HD = HD + Iz{1}*Ix{mm};
                            HD = HD + Iz{1}*Iy{mm};
                            HD = HD + Iz{1}*Iz{mm};
                            
                            HDip = dipC*(3*Iz{1}*Iz{mm}-HD)/2;
                            Hfree_p = Hfree_p + HDip;
                        end
                    end
                
                    % mw pulses    
                    Hnonsel_p = Hfree_p + oneE*Sx;

                    % Integration step for the Signal to account for oscillation
                    if v_off_S==0
                        t2 = 1/abs(off_1*Nint);
                    else
                        t2 = 1/abs(v_off_S*Nint);
                    end



                    U2_p = expm(-1i*Hfree_p*t2); 


                % loop over rf frequencies (x-axis)  
                for a = 1:Npts_EN
                % for a=1   %%% for testing
    
                    % Radiofrequency
                    v_RF = (start_EN+step_EN*(a-1));   
    
                    Hcorr = zeros(size(Hfree_p));
                    HRF = Hfree_p;

                    if opt("Bterm")==false
                       HRF = HRF+ 2*pi*v_RF*Iz{1}+oneN*Iy{1};
                    end

                    Hcorr = Hcorr + 2*pi*v_RF*Iz{1};
                            

                    if opt("Bterm")==false
                        U1 = expm(-1i*HRF*t(1));  
                        U2 = U2_p*expm(-1i*Hcorr*t2);
                    else
                        Hfree = Hfree_p;
                        U1 = RFBterm(opt,expt,v_RF,Hfree,Iy,t(1),Ni_ENDOR,N_spinSys);                            
                        U2 = U2_p;
                    end

    
                    % Evolve the densitymatrix                
                    rho = rho0;
                    rho = U1*rho*U1'; 

                    if opt("Bterm")==true
                        rho = diag(diag(rho));
                    end


                    value_Sy = 0;
                    value_Sy = value_Sy+(real(trace(rho*Sy)));
                    for b=1:Nint*10
                       rho = U2*rho*U2';
                       value_Sy = value_Sy+(real(trace(rho*Sz*Iz{1})));
                    end

                    if opt('temp_eff')==true
                        endor_amp_tmp(a) = endor_amp_tmp(a)+abs(value_Sy*S/(Nint*size(mI,2)))*const_R;
                    else
                        endor_amp_tmp(a) = endor_amp_tmp(a)+abs(value_Sy*S/(Nint*size(mI,2)));

                    end

                end
                end
            end
            endor_amp = endor_amp + endor_amp_tmp;
        end
    end


    function endor_amp = fullTensorCalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual ENDOR calculation (with relaxation)
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  			

        % redefine, one spin system is not possible       
        spinSys('N_SpinSys') = spinSys('Ni_ENDOR');
        spinOps=spin_operators(spinSys("S"),spinSys("I"),spinSys("Ni_ENDOR"),spinSys("Ni_ENDOR"));

        spinOps_D = relaxation.diag_spinOps(spinOps,spinSys('Ni_ENDOR'));

        % get values from Maps
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");

        Sz_D = spinOps_D("Sz");
        Sx_D = spinOps_D("Sx");
        Sy_D = spinOps_D("Sy");
        Iz_D = spinOps_D("Iz");
        Ix_D = spinOps_D("Ix");
        Iy_D = spinOps_D("Iy");

        t = expt("t");
        Nint=8;
        
        
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
            
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel=EPR("NQI_sel");
        CS_zz_sel = EPR("CS_zz_sel");
        

        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        endor_amp = zeros(1,Npts_EN);

        if length(B_sel)==0
            disp('no resonances found')
            return
        end

        % loop to repeat the calculation for every orientation 
        parfor j=1:length(B_sel)

            % set parameters for this orientation
            geff=geff_sel(j);
            B = B_sel(j);
            const_R = 1/size(Sz,2)*constants('GE')*B/(2*pi*constants('K_B')*opt("T"));


            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);
            CS_zz = CS_zz_sel(j,:)*1e-12;

            S = S_sel(j);
            offsets = offsets_sel(j,:);
           
            endor_amp_tmp = zeros(1,Npts_EN);

            % loop over nuclei 
            for i =1 : Ni_ENDOR
                I = spinSys('I');
                mI = -I(i):1:I(i);

                for jj = mI
    
                    v_off_S = jj*HF_zz(i);
                    off_1 = -I(i)*HF_zz(i);
       
                    [rho0] = 2*Sz*Iz{1};

                RT2e = relaxation.RelaxT2(Sx_D,spinSys("T2e"));
                RT2n = zeros(size(RT2e));

                if N_spinSys==1
                    for mm=1:Ni_ENDOR
                       RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{mm},spinSys("T2dq"));
                       RT2n = RT2n+relaxation.RelaxT2(Ix_D{mm},spinSys("T2n"));
                    end
                else
                    RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{1},spinSys("T2dq"));
                    RT2n = RT2n+relaxation.RelaxT2(Ix_D{1},spinSys("T2n"));
                end
                R = RT2e+RT2n;            
                start_EN = paramsENDOR("start_EN");
                step_EN = paramsENDOR("step_EN");
    

                oneN = expt("oneN");

                Hfree_p = 2*pi*v_off_S*Sz;
                if spinSys('N_SpinSys')>1
                    nuc = i;
                    m =1;
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(m)*Iz{1} + 2*pi*v_L(m)*Iz{1}*CS_zz(m)+ 2*pi*HF_zz(m)*(Sz*Iz{1})+ 2*pi*HF_zy(m)*(Sz*Iy{1})+ 2*pi*HF_zx(m)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        Hfree_p = Hfree_p + NQI(m,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(m,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(m,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(m,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(m,3,3)*Iz{1}*Iz{1};
                    else
                        Hfree_p = Hfree_p + pi*NQI_zz(m)*(3*Iz{1} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end                        
                else
                    for mm=1:Ni_ENDOR                            
                        % HF
                        Hfree_p = Hfree_p- 2*pi*v_L(mm)*Iz{mm} + 2*pi*HF_zz(mm)*(Sz*Iz{mm}) + 2*pi*HF_zy(mm)*(Sz*Iy{mm})+ 2*pi*HF_zx(mm)*(Sz*Ix{mm})+2*pi*v_L(mm)*CS_zz(mm)*Iz{mm};
                        % NQI
                        if opt("Bterm")==true 
                            Hfree_p = Hfree_p + NQI(mm,1,1)*Ix{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,2)*Ix{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,1,3)*Ix{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,1)*Iy{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,2)*Iy{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,2,3)*Iy{mm}*Iz{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,1)*Iz{mm}*Ix{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,2)*Iz{mm}*Iy{mm};
                            Hfree_p = Hfree_p + NQI(mm,3,3)*Iz{mm}*Iz{mm};
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 
                        end                                
                    end
                end

                % Integration step for the Signal to account for oscillation
                if v_off_S==0
                    t2 = 1/(off_1*Nint);
                else
                    t2 = 1/(v_off_S*Nint);
                end


                % loop over rf frequencies (x-axis)  
                for a = 1:Npts_EN
                % for a=1   %%% for testing
    
                    % Radiofrequency
                    v_RF = (start_EN+step_EN*(a-1));   
    
                    Hcorr = zeros(size(Hfree_p));
                    HRF = Hfree_p;

                    if opt("Bterm")==false
                        if spinSys('N_SpinSys')>1
                            m=1;
                            Hcorr = Hcorr + 2*pi*v_RF*Iz{m};

                            HRF = HRF+ 2*pi*v_RF*Iz{m}+oneN*Iy{m};
                        else
                            for mm = 1:Ni_ENDOR
                                Hcorr = Hcorr + 2*pi*v_RF*Iz{mm};

                                HRF = HRF+ 2*pi*v_RF*Iz{mm}+oneN*Iy{mm};
                            end
                        end
                    end

                    Hfree = Hfree_p + Hcorr;
                    Hfree = relaxation.Liouville_N2byN2(Hfree);

                    
                    if opt("Bterm")==false

                        U1 = expm((R-1i*relaxation.Liouville_N2byN2(HRF))*t(1));  
                        U2 = expm((R-1i*(Hfree))*t2);
                    else
                        U1 = relaxation.Bterm(opt,expt,v_RF,Hfree_p,Iy,t(1),Ni_ENDOR,N_spinSys,R);
                        U2 = expm((R-1i*(Hfree))*t2);
                    end
    
                    % Evolve the densitymatrix  

                    rho = relaxation.MatrixToLiouvilleBra(rho0)';
                    rho = U1*rho;

                    value_Sy = 0;
                    for b=1
                       rho = U2*rho;
                       rho_f = relaxation.LiouvilleKetToMatrix(rho);
                       value_Sy = value_Sy+(real(trace(rho_f*Sz*Iz{1})));
                    end
                    if opt('temp_eff')==true
                        endor_amp_tmp(a) = endor_amp_tmp(a)+abs(value_Sy*S/(Nint*size(mI,2)))*const_R;
                    else
                        endor_amp_tmp(a) = endor_amp_tmp(a)+abs(value_Sy*S/(Nint*size(mI,2)));

                    end

                end
                end
            end
            endor_amp = endor_amp + endor_amp_tmp;
        end
    end

end
end