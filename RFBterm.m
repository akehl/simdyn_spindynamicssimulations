function U = RFBterm(opt,expt,v_RF,Hfree,Iy,t,Ni_ENDOR,N_spinSys)
    % creates the propagator for the RF pulse if the Bterm is considered
    %
    % input parameters:
    % opt: the Map containing the optional paramters
    % expt: the Map containing the experimental parameters
    % v_RF: RF frequency 
    % Hfree: acting Hamiltonian without RF term
    % Iy: the Iy spin Operator dictionary
    % t: pulse length
    % Ni_ENDOR: number of ENDOR nuclei
    % N_spinSys: number of spin systems
    %
    % output parameters:
    % U: propagator for the pulse
    % 
    % February 2024 A. Kehl (akehl@gwdg.de)

    % Incrementation calculation for the RF pulse
    t_stepRF = 1/(v_RF*opt("N_stepRF"));
    U_RF = eye(size(Hfree));

    for ll = 1:opt("N_stepRF")    
        if N_spinSys==1
            HRF =Hfree;
            for mm=1:Ni_ENDOR
                HRF = HRF + 2* (expt("oneN"))*Iy{mm}*cos(2*pi*v_RF*t_stepRF*(ll-1));
            end
        else
            HRF=Hfree + 2* (expt("oneN"))*Iy{1}*cos(2*pi*v_RF*t_stepRF*(ll-1));
        end
        U_RF = expm(-1i*HRF*t_stepRF)*U_RF;
    end

    U = (U_RF^(t*v_RF));

end
