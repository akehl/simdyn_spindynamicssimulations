classdef defineExp
    % functions to define the experimental parameters
    % 
    % February 2024 A. Kehl (akehl@gwdg.de)

    methods(Static=true)
    function Expt = create(freqMeas,field,deltaField,res_EN,range_EN,t)
        % creates the Map with experimental parameters
        %
        % input parameters:
        % freqMeas: measurement frequency (MW) in GHz
        % field: main field for ENDOR exp. in G
        % deltaField: EPR step size in G
        % res_EN: resolution in ENDOR exp. in MHz
        % range_EN: ENDOR exp. range in MHz
        % t: pulse times in ns
        %
        % output parameters:
        % Expt: the Map with the experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %

        Expt = containers.Map;
    
        Expt("FreqMeas")= freqMeas*10^9;                     % GHz to Hz
        Expt("Field") = field*10^-4;                         % G to T
        Expt("deltaField") = deltaField*10^-4;               % G to T
        Expt("res_EN") = res_EN*10^6;                        % MHz to Hz
        Expt("range_EN") = range_EN*10^6;
    
        Expt("t") = t*10^-9;                                 % ns to s
    
    end
    
    function Expt_out = freq(expt_in,freq_min,freq_range,freq_steps)
        % adds experimental parameters for freq domain EPR calculations
        %
        % input parameters:
        % expt_in: the Map with the experimental parameters
        % freq_min: minimal sweep freq in GHz
        % freq_range: range of freq sweep in GHz
        % freq_steps: steps of freq sweep 
        %
        % output parameters:
        % Expt_out: updated Map with the experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %
        Expt_out = expt_in;
        
        Expt_out("FreqMin") = freq_min * 1e9;
        Expt_out("FreqRange") = freq_range * 1e9;
        Expt_out("FreqSteps") = freq_steps * 1e9;
    
    end

    function Expt_out = setExciteWidth(expt_in, value)
        % defines excitation bandwidth
        %
        % input parameters:
        % expt_in: the Map with the experimental parameters
        % value: excitation bandwidth in MHz
        %
        % output parameters:
        % Expt_out: updated Map with the experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %

        Expt_out = expt_in;
        Expt_out("exciteWidth") = value*10^6; % [MHz to Hz]
    end

    function Expt_out = pulseInput(expt_in,data,multipulses)
        % sets pulse profile input file
        %
        % input parameters:
        % expt_in: the Map with the experimental parameters
        % data: filename of pulse profile
        %
        % output parameters:
        % Expt_out: updated Map with the experimental parameters
        %
        % April 2024 A. Kehl (akehl@gwdg.de)
        %

        Expt_out = expt_in;

        Expt_out("pulse")=data;
        Expt_out("3pulses")=multipulses;
    end

    function Expt_out = RFstart(expt_in, value)
        % sets RF start frequency
        %
        % input parameters:
        % expt_in: the Map with the experimental parameters
        % value: RF start frequency in MHz
        %
        % output parameters:
        % Expt_out: updated Map with the experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %

        Expt_out = expt_in;

        Expt_out("RF_start") = value*1e6;
    end

    function Expt_out = setAng(expt_in, value)
        % defines turning angle of RF pulse
        %
        % input parameters:
        % expt_in: the Map with the experimental parameters
        % value: turning angle of pulse in degree
        %
        % output parameters:
        % Expt_out: updated Map with the experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %
        
        Expt_out = expt_in;
        Expt_out("ang") = value;
    end

    end
end