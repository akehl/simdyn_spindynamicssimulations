classdef relaxation
    % functions to calculate relaxation effects
    % based on script by S. Vega 
    %
    % February 2024 A. Kehl (akehl@gwdg.de)

methods(Static=true)
    function spinSys_out = defineRelax(spinSys_in,T1e,T1n,T2e,T2n,T2dq)
        % adds the relaxation parameters to the spinsystem
        % is directly called for the set up
        % 
        % input parameters:
        % spinSys_in: the Map describing the spin system 
        % T1e: T1e relaxation time in s
        % T1n: T1n relaxation time in s
        % T2e: T2e relaxation time in s
        % T2n: T2n relaxation time in s
        % T2dq: T2 double and zero quantum time in s
        %
        % output parameters:
        % spinSys_out: updated Map describing the spin system 
        %
        % February 2024 A. Kehl (akehl@gwdg.de)

        arguments
            spinSys_in
            T1e
            T1n
            T2e
            T2n
            T2dq
        end
        
        spinSys_out = spinSys_in;

        spinSys_out('T1e') = T1e;
        spinSys_out('T1n') = T1n;
        spinSys_out('T2e') = T2e;
        spinSys_out('T2n') = T2n;
        spinSys_out('T2dq') = T2dq;
        
    end

    function spinSys_out = defineRelax_withRho(spinSys_in,T1e,T1n,T2e,T2n,T2dq,T1eRho,T1nRho,T2eRho,T2nRho,T2dqRho)
        % adds the relaxation parameters to the spinsystem
        % if a tilted frame (rho) needs t be considered, i.e. CP/SL ENDOR
        % is directly called for the set up
        % 
        % input parameters:
        % spinSys_in: the Map describing the spin system 
        % T1e: T1e relaxation time in s
        % T1n: T1n relaxation time in s
        % T2e: T2e relaxation time in s
        % T2n: T2n relaxation time in s
        % T2dq: T2 double and zero quantum time in s
        %
        % output parameters:
        % spinSys_out: updated Map describing the spin system 
        %
        % April 2024 A. Kehl (akehl@gwdg.de)
        
        spinSys_out = spinSys_in;

        spinSys_out('T1e') = T1e;
        spinSys_out('T1n') = T1n;
        spinSys_out('T2e') = T2e;
        spinSys_out('T2n') = T2n;
        spinSys_out('T2dq') = T2dq;
        spinSys_out('T1eR') = T1eRho;
        spinSys_out('T1nR') = T1nRho;
        spinSys_out('T2eR') = T2eRho;
        spinSys_out('T2nR') = T2nRho;
        spinSys_out('T2dqR') = T2dqRho;
        
    end

    function Ops = diag_spinOps(spinOps,N_Nuclei)
        % generate diagonal spin operators 
        % 
        % input parameters:
        % spinOps: the Map containing the spin operators
        % N_Nuclei: number of nuclei
        %
        % output parameters:
        % Ops: the Map containing the diagonal spin operators
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        Ops = containers.Map;
        D = eye(size(spinOps('Sz')));


        Ops('Sx') = D\spinOps('Sx')*D;
        Ops('Sy') = D\spinOps('Sy')*D;
        Ops('Sz') = D\spinOps('Sz')*D;

        Ix = spinOps('Ix');
        Iy = spinOps('Iy');
        Iz = spinOps('Iz');

        Ix_D{N_Nuclei} = [];
        Iy_D{N_Nuclei} = [];
        Iz_D{N_Nuclei} = [];

        for i = 1:size(Ix,2)
            Ix_D{i} = D/Ix{i}*D;
            Iy_D{i} = D/Iy{i}*D;
            Iz_D{i} = D/Iz{i}*D;
        end

        Ops('Ix') = Ix_D;
        Ops('Iy') = Iy_D;
        Ops('Iz') = Iz_D;        
    end

    function Ml=Liouville_N2byN2(Mh)
        % transforms Hamiltonian defined for Hilbert space to one for
        % Liouville space
        %
        % input parameters:
        % Mh: the Hamiltonian to be transformed
        % 
        % output parameters:
        % Ml: the Hamiltonian in Liouville space
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        s=size(Mh,1);
        Ml=zeros(s^2,s^2);                
        for m1=1:s
            for n=1:s %==n1 or n2
                for m2=1:s
                    %m2==n2. n==n1
                    Ml(m1+(m2-1)*s, n+(m2-1)*s)=Ml(m1+(m2-1)*s, n+(m2-1)*s)+Mh(m1,n);
                    %m1==n1. n==n2
                    Ml(m1+(m2-1)*s, m1+(n-1)*s)=Ml(m1+(m2-1)*s, m1+(n-1)*s)-Mh(n,m2);%+Mh(m2,n);
                    
                end
            end
        end
    end

    function Mhilb = LiouvilleKetToMatrix(Ml)
        % transforms Liouville Ket to Hilbert space matrix
        %
        % input parameters:
        % Ml: the Liouville space Ket
        % 
        % output parameters:
        % Mh: the Hilbert space matrix
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        hilbSize=sqrt(size(Ml,1));
                
        for n=1:1:hilbSize
            for m=1:1:hilbSize
                Mhilb(n,m)=Ml(n+(m-1)*hilbSize);
            end
        end
    end

    function Ml=MatrixToLiouvilleBra(Mhilb)
        % transforms Hilbert space matrix to Liouville Bra 
        %
        % input parameters:
        % Mh: the Hilbert space matrix
        % 
        % output parameters:
        % Ml: the Liouville space Ket
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        hilbSize=size(Mhilb,1);
        
        for n=1:1:hilbSize
            for m=1:1:hilbSize
                Ml(n+(m-1)*hilbSize)=Mhilb(m,n);
            end
        end

    end

    function R1 = RelaxT1(rho0,O,T1)
        % generates the relaxation superoperator for T1 relaxation in
        % Liouville space
        %
        % input parameters:
        % rho0: the equilibrium density matrix
        % O: the Relaxation inducing operator (usually Sx or Ix)
        % T1: the relaxation time in s
        %
        % output parameters:
        % R1: the R1 relaxation superoperator
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        % preallocate matrix
        R1=zeros(size(O).^2);

        % equilibrium population of states are diagonal values of rho0
        population=diag(rho0);
        % epsilon gives the ratio between the equl. densities 
        epsilon=(population*population'.^-1)';
        
        % equivalent to the epsilon factor in 
        % Feintuch, Vega in EPR spectroscopy (2018)
        % (only upper right triangle used later)
        Thermal=epsilon./(1+epsilon)./T1;
        Thermal2=1./(1+epsilon)./T1;
    
        S = length(O);
        [j k]=find(triu(O)); 
    
        nj=(j+(j-1)*S);
        nk=(k+(k-1)*S);
    
        M=size(j);
        % add relaxation values for all spins 
        for m=1:M
            R1(nj(m),nj(m))=R1(nj(m),nj(m))-Thermal(j(m),k(m));
            R1(nk(m),nj(m))=R1(nk(m),nj(m))+Thermal(j(m),k(m));
            R1(nj(m),nk(m))=R1(nj(m),nk(m))+Thermal2(j(m),k(m));
            R1(nk(m),nk(m))=R1(nk(m),nk(m))-Thermal2(j(m),k(m));
        end
    end

    function RT2 =RelaxT2(O,T2)
        % generates the relaxation superoperator for T1 relaxation in
        %
        % input parameters:
        % O: the Relaxation inducing operator (usually Sx or Ix)
        % T2: the relaxation time in s
        %
        % output parameters:
        % RT2: the R2 relaxation superoperator
        % 
        % February 2024 A. Kehl (akehl@gwdg.de)

        RT2 = zeros(size(O).^2);
         
        ind = find(relaxation.MatrixToLiouvilleBra(O));
        
        RT2(ind,ind) = -1 / T2;
        RT2 = diag(diag(RT2));
    end

    function U = Bterm(opt,expt,v_RF,Hfree,Iy,t,Ni_ENDOR,N_spinSys,R)
        % creates propagator for RF pulse with Bterm when relaxation is
        % included
        %
        % input [arameters:
        % opt: the Map containing the optional paramters
        % expt: the Map containing the experimental parameters
        % v_RF: RF frequency 
        % Hfree: acting Hamiltonian without RF term
        % Iy: the Iy spin Operator dictionary
        % t: pulse length
        % Ni_ENDOR: number of ENDOR nuclei
        % N_spinSys: number of spin systems
        % R: the relaxation superoperator
        %
        % output parameters:
        % U: propagator for the pulse
        %
        % February 2024 A. Kehl (akehl@gwdg.de)


        % Incrementation calculation for the RF pulse
        t_stepRF = 1/(v_RF*opt("N_stepRF"));
        U_RF = eye(size(R));
        
    
        for ll = 1:opt("N_stepRF")    
            if N_spinSys==1
                HRF =Hfree;
                for mm=1:Ni_ENDOR
                    HRF = HRF - 2* (expt("oneN"))*Iy{mm}*cos(2*pi*v_RF*t_stepRF*(ll-1));
                end
            else
                HRF=Hfree - 2* (expt("oneN"))*Iy{1}*cos(2*pi*v_RF*t_stepRF*(ll-1));
            end
            U_RF = expm((R-1i*relaxation.Liouville_N2byN2(HRF))*t_stepRF)*U_RF;
        end
    
        U = (U_RF^(t*v_RF));
    end
end
end
