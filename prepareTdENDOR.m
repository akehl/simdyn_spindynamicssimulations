function paramsENDOR = prepareTdENDOR(constants,spinSys,expt)
    % calculates/sets necessary parameters for the time domain ENDOR calculation from
    % the experimental values
    % 
    % input parameters:
    % constants:the Map containing the constants
    % spinSys: the Map describing the spin system 
    % expt: the Map containing the experimental parameters
    % 
    % output parameters:
    % paramsENDOR: the Map containing the ENDOR parameters
    % 
    % February 2024 A. Kehl (akehl@gwdg.de)

    % Larmor frequencies
    Nuclei=spinSys("Nuclei");
    v_L=zeros(size(Nuclei,2),1);
    for i=1:size(Nuclei,2)
        if strcmp(Nuclei(i),"1H")           
            v_L(i) = constants("GN_1H")*expt("Field");                    % Hz    Nuclar larmor frequency             
        elseif strcmp(Nuclei(i),"2D")    
            v_L(i) = constants("GN_2D")*expt("Field");                    % Hz    Nuclar larmor frequency 
        elseif strcmp(Nuclei(i),"14N")    
            v_L(i) = constants("GN_14N")*expt("Field");                   % Hz    Nuclar larmor frequency 
        elseif strcmp(Nuclei(i),"17O")    
            v_L(i) = constants("GN_17O")*expt("Field");                   % Hz    Nuclar larmor frequency 
        elseif strcmp(Nuclei(i),"19F")    
            v_L(i) = constants("GN_19F")*expt("Field");                   % Hz    Nuclar larmor frequency 
        end
    end

    % number of points and values for x-axis
    range_EN = expt("range_EN")*1e-12;       % input in us, factor 10e-6 due to usual conversion MHz to Hz
    res_EN = expt("res_EN")*1e-12;           % input in us, factor 10e-6 due to usual conversion MHz to Hz
    Npts_EN = (round(range_EN/res_EN));  
    start_EN = expt("t_start");                        % ENDOR start x-axis

    step_EN = range_EN/(Npts_EN-1);    % X-Axis steps
    x_coords = zeros(Npts_EN);
    for ii = 1:Npts_EN
        x_coords(ii) = start_EN + (ii-1)*step_EN;  %-v_L;
    end
               
    paramsENDOR = containers.Map;

    paramsENDOR("v_L") = v_L;
    paramsENDOR("start_EN") = start_EN;
    paramsENDOR("step_EN") = step_EN;
    paramsENDOR("range_EN") = expt("range_EN");
    paramsENDOR("Npts_EN") = Npts_EN;
    paramsENDOR("x_coords") = x_coords;



end