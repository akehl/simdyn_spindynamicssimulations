classdef TimeDomain
    % functions for the Mims like time domain ENDOR experiment
    % this is still a beta version and further development/testing is
    % required
    %
    % April 2024 A. Kehl (akehl@gwdg.de)
    %   
methods (Static=true)
    function expt = setFieldStrength(constants,expt,set,values)
        % set the mw and rf field strength (omega1e, etc.)
        % is directly called for the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % expt: the Map containing the experimental parameters
        % set: boolean, should the field strength be calculated from the
        %       pulse length
        % values: if not fixed (set=true), array with field strengths
        %
        % output parameters:
        % expt: updated Map with experimental parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %   
        arguments
            constants
            expt
            set
            values=[]
        end

        % setting the field strength values as needed for the time domain ENDOR experiment
        if set==false
            if size(values,1)==2
                expt("oneE") = values(1)*2*pi*1e6;
                expt("oneN") = values(2)*2*pi*1e3;

                % omega1e in T for orientation preselection 
                expt("pulsewidth") = expt("oneE")/(2*pi*constants("CONST1")*1e10);        
            elseif values~=[]
                println("Wrong dimensions of input, should be: [omega_prep/2pi,omega_1e/2pi,omega_1n/2pi]");
            end
        else
            t = expt("t");
            expt("oneE") = 2*pi/(4*t(1));
            if isKey(expt,"ang")
                expt("oneN") = (expt("ang")/180)*2*pi/(4*t(5));
                disp(expt("oneN"))
            else
                expt("oneN") = 2*pi/(4*t(5));
                disp(expt("oneN"))
            end

            % omega1e in T for orientation preselection 
            expt("pulsewidth") = expt("oneE")/(2*pi*constants("CONST1")*1e10);
        end
    end

    function [endor_amp,endor_amp_conv,x_coords,v_L] = ENDOR(constants,spinSys,spinOps,expt,opt)
        % start time domain  ENDOR calculation
        % is directly called from the set up
        % 
        % input parameters:
        % constants: the Map containing the constants
        % spinSys: the Map describing the spin system 
        % spinOps: the Map containing the spin operators
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        %
        % output parameters:
        % endor_amp: endor amplitude values
        % endor_amp_conv: endor values convoluted with lb
        % x_coords: coords of rf axis
        % v_L: nuclear Larmor frequency
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  

        t = expt('t');
        expt('t_start') = t(6);   
        
        % prepare EPR and ENDOR calculations 
        paramsEPR   = prepareEPR(spinSys,expt);
        paramsENDOR = prepareTdENDOR(constants,spinSys,expt);
        
        x_coords = paramsENDOR("x_coords");
        x_coords = x_coords(:,1)';
        v_L = paramsENDOR("v_L");
     

        % EPR calculation
        if opt('freqDomain')==false
            epr = calcOriFieldDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        else
            epr = calcOriFreqDomain(constants,spinSys,spinOps,paramsEPR,paramsENDOR,opt,expt); 
        end
            
        % ENDOR calculation
        if opt("Relax") == true
            endor_amp = TimeDomain.ENDORcalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation with relaxation
        else
            endor_amp = TimeDomain.ENDORcalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,epr);     % starts the actual calculation
        end
        
        % convolution with lb
        endor_amp_conv = lb(endor_amp,opt);
    end

    function endor_amp = ENDORcalc(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual ENDOR calculation (no relaxation)
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        %  	

        % get values from Maps
        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");
        t = expt("t");
        Nint=8;
        
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
           
        
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel = EPR("NQI_sel");
        CS_zz_sel = EPR("CS_zz_sel");
        D_zz_sel = EPR("D_zz_sel");

        if isKey(spinSys,"CS")
           [v_cs,d_cs] = eig(spinSys("CS"));
           cs_iso = trace(d_cs)/3*1e-12;
        else
            cs_iso=0;
        end


        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        endor_amp = zeros(1,Npts_EN);

        if length(B_sel)==0
            disp('no resonances found')
            return
        end


        % loop to repeat the calculation for every orientation 
        parfor j=1:length(B_sel)
            % for j=1
            % set parameters for this orientation
            endor_amp_tmp = zeros(1,Npts_EN);

            geff=geff_sel(j);
            B = B_sel(j);

            HF_zz = HF_zz_sel(j,:);
            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);
            
            CS_zz = CS_zz_sel(j,:)*1e-12;
            D_zz = D_zz_sel(j,:);

     
            S = S_sel(j);
            offsets = offsets_sel(j,:);
            

            % loop over all offsets (spin manifolds)
            for i = 1 : size(offsets,2) 
    
                v_off_S = offsets(i);
                off_1 = offsets(1);
   
                [rho0] = getRho0(constants,paramsENDOR,B,geff,spinOps,spinSys,opt,HF_zz,HF_zy,HF_zx,NQI_zz);

                start_EN = paramsENDOR("start_EN");
                step_EN = paramsENDOR("step_EN");
    
                oneE = expt("oneE");
                oneN = expt("oneN");

                Hfree_p = 2*pi*v_off_S*Sz;
                if spinSys('N_SpinSys')>1
                    nuc = round(i/(2*I(1)+1)) ;
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(1)*Iz{1} + 2*pi*v_L(1)*Iz{1}*CS_zz(nuc)+ 2*pi*HF_zz(nuc)*(Sz*Iz{1})+ 2*pi*HF_zy(nuc)*(Sz*Iy{1})+ 2*pi*HF_zx(nuc)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        Hfree_p = Hfree_p + NQI(1,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(1,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(1,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,3,3)*Iz{1}*Iz{1};
                    else
                        Hfree_p = Hfree_p + pi*NQI_zz(1)*(3*Iz{1} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end                
                else
                    for mm=1:Ni_ENDOR                            
                        % HF
                        Hfree_p = Hfree_p- 2*pi*v_L(mm)*Iz{mm} + 2*pi*HF_zz(mm)*(Sz*Iz{mm}) + 2*pi*HF_zy(mm)*(Sz*Iy{mm})+ 2*pi*HF_zx(mm)*(Sz*Ix{mm})+2*pi*v_L(mm)*CS_zz(mm)*Iz{mm};
                        % NQI
                        if opt("Bterm")==true 
                            HNQI = zeros(size(Hfree_p));
                            HNQI = HNQI + Ix{mm}*NQI(mm,1,1)*Ix{mm};
                            HNQI = HNQI + Ix{mm}*NQI(mm,1,2)*Iy{mm};
                            HNQI = HNQI + Ix{mm}*NQI(mm,1,3)*Iz{mm};
                            HNQI = HNQI + Iy{mm}*NQI(mm,2,1)*Ix{mm};
                            HNQI = HNQI + Iy{mm}*NQI(mm,2,2)*Iy{mm};
                            HNQI = HNQI + Iy{mm}*NQI(mm,2,3)*Iz{mm};
                            HNQI = HNQI + Iz{mm}*NQI(mm,3,1)*Ix{mm};
                            HNQI = HNQI + Iz{mm}*NQI(mm,3,2)*Iy{mm};
                            HNQI = HNQI + Iz{mm}*NQI(mm,3,3)*Iz{mm};
                            Hfree_p = Hfree_p +HNQI;
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mm)*(3*Iz{mm} *Iz{mm} -I(mm) *(I(mm) +1)*eye(size(Hfree_p))); 

                        end

                    end

                    if spinSys("D_used")==true
                        for mm = 2:(size(D_zz,2)+1)
                            dipC = 2*pi*D_zz(mm-1);
    
                            HD = zeros(size(Hfree_p));

                            HD = HD + Ix{1}*Ix{mm};
                            HD = HD + Ix{1}*Iy{mm};
                            HD = HD + Ix{1}*Iz{mm};
                            HD = HD + Iy{1}*Ix{mm};
                            HD = HD + Iy{1}*Iy{mm};
                            HD = HD + Iy{1}*Iz{mm};
                            HD = HD + Iz{1}*Ix{mm};
                            HD = HD + Iz{1}*Iy{mm};
                            HD = HD + Iz{1}*Iz{mm};
                            
                            HDip = dipC*(3*Iz{1}*Iz{mm}-HD)/2;

                            Hfree_p = Hfree_p + HDip;
                        end
                    end
                end
                    v_RF = v_L(1);
                    Hcorr = zeros(size(Hfree_p));
                    if spinSys('N_SpinSys')>1
                        m=1;
                        Hcorr = Hcorr + 2*pi*v_RF*Iz{m};
                    else

                        for mm = 1:Ni_ENDOR
                            Hcorr = Hcorr + 2*pi*v_RF*Iz{mm};
                            
                        end
                    end

                    if opt("Bterm")==false
                        Hfree = Hfree_p + Hcorr;
                    else
                        Hfree = Hfree_p;
                    end

                    % mw pulses    
                    Hnonsel = Hfree + oneE*Sx;
                    
                    HRF = Hfree;

                    if opt("Bterm")==false
                        if spinSys('N_SpinSys')>1
                            m=1;
                            HRF = HRF+oneN*Iy{m};
                        else
                            for mm = 1:Ni_ENDOR
                                HRF = HRF+oneN*Iy{mm};
                            end
                        end
                    end
                    

                    % Integration step for the Signal to account for oscillation
                    if v_off_S==0
                        t11 = 1/abs(off_1*Nint);
                    else
                        t11 = 1/abs(v_off_S*Nint);
                    end


                    if opt('Bterm')==false
                        U5 = expm(-1i*HRF*t(5));  
                        if t(5)==t(7)
                            U7 = U5;
                        else
                            U7 = expm(-1i*HRF*t(7));                           
                        end

                    else
                        U5 = RFBterm(opt,expt,v_RF,Hfree,Iy,t(5),Ni_ENDOR,N_spinSys);
                        if t(5)==t(7)
                            U7 = U5;
                        else
                            U7 = RFBterm(opt,expt,v_RF,Hfree,Iy,t(7),Ni_ENDOR,N_spinSys);
                        end
                    end

                    % Calculate the propagators
                    U1 = expm(-1i*Hnonsel*t(1));

                    if t(1)==t(3)
                        U3 = U1;
                    else
                        U3 = expm(-1i*Hnonsel*t(3));                           
                    end
                    
                    if t(1)==t(9)
                        U9 = U1;
                    elseif t(3)==t(9)
                        U9=U3;
                    else
                        U9 = expm(-1i*Hfree*t(9)); 
                    end

                    U2 = expm(-1i*Hfree*t(2));
         
                    U4 = expm(-1i*Hfree*t(4)); 
                  
                    U10 = expm(-1i*Hfree*(t(2)+t(3)/2));

                    U11 = expm(-1i*Hfree*t11); 
                    

                    % Evolve the densitymatrix                
                    rho = rho0;
                    rho = U1*rho*U1'; 
                    rho = U2*rho*U2';           
                    rho = U3*rho*U3'; 
                    rho = U4*rho*U4';
                    rho = U5*rho*U5'; 

                    if opt("Bterm")==true
                        rho = diag(diag(rho));
                    end

                rho_t = rho;

                 % loop over different separation of the RF pulses (x-axis)  
                for a = 1:Npts_EN    
                    t6= t(6)+step_EN*(a-1);
                    t8= t(8)-step_EN*(a-1);


                    if opt("Bterm")==false
                        U6 = expm(-1i*(Hfree_p+Hcorr)*t6);
                    else
                        Hfree = Hfree_p;
                        U6 = expm(-1i*(Hfree)*t6);
                    end
                    U8 = expm(-1i*Hfree*t8);


                    rho = rho_t;
                    rho = U6*rho*U6';
                    rho = U7*rho*U7';

                    if opt("Bterm")==true
                        rho = diag(diag(rho));
                    end

                    rho = U8*rho*U8';
                    rho = U9*rho*U9';    
                    rho = U10*rho*U10';

                    value_Sy = 0;
                    for b=1:Nint
                       rho = U11*rho*U11';
                       value_Sy = value_Sy+(real(trace(rho*Sy)));
                    end

                    endor_amp_tmp(a) = endor_amp_tmp(a)+(value_Sy*S/(Nint*size(offsets,2)));
                end
            end
            endor_amp = endor_amp+endor_amp_tmp;
        end

    end


    function endor_amp = ENDORcalc_relax(constants,spinOps,spinSys,expt,opt,paramsENDOR,EPR)
        % performs the actual ENDOR calculation (with relaxation)
        % CAUTION: this is a beta version and not fully tested
        %
        % input parameters:
        % constants: the Map containing the constants
        % spinOps: the Map containing the spin operators
        % spinSys: the Map describing the spin system 
        % expt: the Map containing the experimental parameters
        % opt: the Map containing the optional paramters
        % paramsENDOR: the Map containing the ENDOR parameters
        % EPR: the Map with results from EPR calculation
        %
        % output parameters:
        % endor_amp: endor amplitude values
        %
        % April 2024 A. Kehl (akehl@gwdg.de)
        %  			

        % get values from Maps
        spinOps_D = relaxation.diag_spinOps(spinOps,spinSys('Ni_ENDOR'));

        Sz = spinOps("Sz");
        Sx = spinOps("Sx");
        Sy = spinOps("Sy");
        Iz = spinOps("Iz");
        Ix = spinOps("Ix");
        Iy = spinOps("Iy");

        Sz_D = spinOps_D("Sz");
        Sx_D = spinOps_D("Sx");
        Sy_D = spinOps_D("Sy");
        Iz_D = spinOps_D("Iz");
        Ix_D = spinOps_D("Ix");
        Iy_D = spinOps_D("Iy");

        t = expt("t");
        Nint=8;
        
        N_spinSys = spinSys("N_SpinSys");
        
        Ni_ENDOR = spinSys("Ni_ENDOR");
        
        if N_spinSys >1
           nucs = Ni_ENDOR;
            for i=1:Ni_ENDOR-1
                Ix{i+1}=Ix{1};
                Iy{i+1}=Iy{1};
                Iz{i+1}=Iz{1};
            end
        else
           nucs = 1;
        end
            
        geff_sel = EPR("geff_sel");
        B_sel = EPR("B_sel");
        HF_zz_sel=EPR("HF_zz_sel");
        HF_zy_sel=EPR("HF_zy_sel");
        HF_zx_sel=EPR("HF_zx_sel");

        NQI_zz_sel=EPR("NQI_zz_sel");
        NQI_sel=EPR("NQI_sel");
        CS_zz_sel = EPR("CS_zz_sel");
        D_zz_sel = EPR("D_zz_sel");

        S_sel=EPR("S_sel");

        
        offsets_sel = EPR("offsets");
        Npts_EN= paramsENDOR("Npts_EN");

        I = spinSys("I");
        v_L = paramsENDOR("v_L");

    
        endor_amp = zeros(1,Npts_EN);
        if length(B_sel)==0
            disp('no resonances found')
            return
        end
        dips("CAUTION: this is a beta version and not fully tested");


        % loop to repeat the calculation for every orientation 
        parfor j=1:length(B_sel)
            endor_amp_tmp = zeros(1,Npts_EN);

            % set parameters for this orientation
            geff=geff_sel(j);
            B = B_sel(j);
            
            HF_zz = HF_zz_sel(j,:);

            HF_zy = HF_zy_sel(j,:);
            HF_zx = HF_zx_sel(j,:);
    
            NQI_zz = NQI_zz_sel(j,:);
            NQI=zeros(Ni_ENDOR,3,3);

            NQI(:,:,:) = 2*pi*NQI_sel(j,:,:,:);
            CS_zz = CS_zz_sel(j,:)*1e-12;
            D_zz = D_zz_sel(j,:);

            S = S_sel(j);
            offsets = offsets_sel(j,:);


            % loop over all offsets (spin manifolds)
            for i = 1 : size(offsets,2) 
    
                v_off_S = offsets(i);
                off_1 = offsets(1);
   
                rho0 = getRho0(constants,paramsENDOR,B,geff,spinOps,spinSys,opt,HF_zz,HF_zy,HF_zx,NQI_zz);

                RT1e = relaxation.RelaxT1(rho0,Sx_D,spinSys("T1e")); %electron T1
                RT1n = zeros(size(RT1e));
                RT2e = relaxation.RelaxT2(Sx_D,spinSys("T2e"));
                RT2n = zeros(size(RT2e));

                if N_spinSys==1
                    for mn=1:Ni_ENDOR
                       RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{mn},spinSys("T1n")); %nuclear T1
                       RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{mn},spinSys("T2dq"));
                       RT2n = RT2n+relaxation.RelaxT2(Ix_D{mn},spinSys("T2n"));
                    end
                else
                    RT1n = RT1e+relaxation.RelaxT1(rho0,Ix_D{1},spinSys("T1n"));
                    RT2e = RT2e+relaxation.RelaxT2(Sx_D*Ix_D{1},spinSys("T2dq"));
                    RT2n = RT2n+relaxation.RelaxT2(Ix_D{1},spinSys("T2n"));
                end
                R = RT1e+RT1n+RT2e+RT2n;

            
                start_EN = paramsENDOR("start_EN");
                step_EN = paramsENDOR("step_EN");
    
                oneE = expt("oneE");
                oneN = expt("oneN");

                Hfree_p = 2*pi*v_off_S*Sz;
                if spinSys('N_SpinSys')>1
                    nuc = round(i/(2*I(1)+1)) ;
                    % HF
                    Hfree_p = Hfree_p- 2*pi*v_L(1)*Iz{1} + 2*pi*v_L(1)*Iz{1}*CS_zz(nuc)+ 2*pi*HF_zz(nuc)*(Sz*Iz{1})+ 2*pi*HF_zy(nuc)*(Sz*Iy{1})+ 2*pi*HF_zx(nuc)*(Sz*Ix{1});
                    % NQI
                    if opt("Bterm")==true 
                        Hfree_p = Hfree_p + NQI(1,1,1)*Ix{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,1,2)*Ix{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,1,3)*Ix{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(1,2,1)*Iy{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,2,2)*Iy{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,2,3)*Iy{1}*Iz{1};
                        Hfree_p = Hfree_p + NQI(1,3,1)*Iz{1}*Ix{1};
                        Hfree_p = Hfree_p + NQI(1,3,2)*Iz{1}*Iy{1};
                        Hfree_p = Hfree_p + NQI(1,3,3)*Iz{1}*Iz{1};
                    else
                        Hfree_p = Hfree_p + pi*NQI_zz(1)*(3*Iz{1} *Iz{1} -I(1) *(I(1) +1)*eye(size(Hfree_p))); 
                    end                        
                else
                    for mn=1:Ni_ENDOR                            
                        % HF
                        Hfree_p = Hfree_p- 2*pi*v_L(mn)*Iz{mn} + 2*pi*HF_zz(mn)*(Sz*Iz{mn}) + 2*pi*HF_zy(mn)*(Sz*Iy{mn})+ 2*pi*HF_zx(mn)*(Sz*Ix{mn})+2*pi*v_L(mn)*CS_zz(mn)*Iz{mn};
                        % NQI
                        if opt("Bterm")==true 
                            Hfree_p = Hfree_p + NQI(mn,1,1)*Ix{mn}*Ix{mn};
                            Hfree_p = Hfree_p + NQI(mn,1,2)*Ix{mn}*Iy{mn};
                            Hfree_p = Hfree_p + NQI(mn,1,3)*Ix{mn}*Iz{mn};
                            Hfree_p = Hfree_p + NQI(mn,2,1)*Iy{mn}*Ix{mn};
                            Hfree_p = Hfree_p + NQI(mn,2,2)*Iy{mn}*Iy{mn};
                            Hfree_p = Hfree_p + NQI(mn,2,3)*Iy{mn}*Iz{mn};
                            Hfree_p = Hfree_p + NQI(mn,3,1)*Iz{mn}*Ix{mn};
                            Hfree_p = Hfree_p + NQI(mn,3,2)*Iz{mn}*Iy{mn};
                            Hfree_p = Hfree_p + NQI(mn,3,3)*Iz{mn}*Iz{mn};
                        else
                            Hfree_p = Hfree_p + pi*NQI_zz(mn)*(3*Iz{mn} *Iz{mn} -I(mn) *(I(mn) +1)*eye(size(Hfree_p))); 
                        end                                
                    end

                    if spinSys("D_used")==true
                        for mn = 2:(size(D_zz,2)+1)
                            dipC = 2*pi*D_zz(mn-1);
    
                            HD = zeros(size(Hfree_p));
    
                            HD = HD + Ix{1}*Ix{mn};
                            HD = HD + Ix{1}*Iy{mn};
                            HD = HD + Ix{1}*Iz{mn};
                            HD = HD + Iy{1}*Ix{mn};
                            HD = HD + Iy{1}*Iy{mn};
                            HD = HD + Iy{1}*Iz{mn};
                            HD = HD + Iz{1}*Ix{mn};
                            HD = HD + Iz{1}*Iy{mn};
                            HD = HD + Iz{1}*Iz{mn};
                            
                            HDip = dipC*(3*Iz{1}*Iz{mn}-HD)/2;
                            Hfree_p = Hfree_p + HDip;
                        end
                    end
                end

                % Integration step for the Signal to account for oscillation
                if v_off_S==0
                    t11 = 1/(off_1*Nint);
                else
                    t11 = 1/(v_off_S*Nint);
                end

                % Radiofrequency
                v_RF = v_L(1); 

                    Hcorr = zeros(size(Hfree_p));
                    HRF = Hfree_p;

                    if opt("Bterm")==false
                        if spinSys('N_SpinSys')>1
                            m=1;
                            Hcorr = Hcorr + 2*pi*v_RF*Iz{m};

                            HRF = HRF+ 2*pi*v_RF*Iz{m}+oneN*Iy{m};
                        else
                            for mn = 1:Ni_ENDOR
                                Hcorr = Hcorr + 2*pi*v_RF*Iz{mn};

                                HRF = HRF+ 2*pi*v_RF*Iz{mn}+oneN*Iy{mn};
                            end
                        end
                    end

                    Hfree = Hfree_p + Hcorr;
                    
                    Hnonsel = relaxation.Liouville_N2byN2(Hfree + oneE*Sx);

                    Hfree = relaxation.Liouville_N2byN2(Hfree);

                    

                    if opt("Bterm")==false
                    
                        U5 = expm((R-1i*relaxation.Liouville_N2byN2(HRF))*t(5));  
                        if t(5)==t(7)
                            U7=U5;
                        else
                            U7 = expm((R-1i*relaxation.Liouville_N2byN2(HRF))*t(7));  
                        end
                        
                        U1 = expm((R-1i*(Hnonsel))*t(1));

                        if t(1)==t(3)
                            U3 = U1;
                        else
                            U3 = expm((R-1i*(Hnonsel))*t(3));
                        end
                        
                        if t(1)==t(9)
                            U9 = U1;
                        elseif t(3)==t(9)
                            U9=U3;
                        else
                            U9 = expm((R-1i*(Hnonsel))*t(9));
                        end

                        U2 = expm((R-1i*(Hfree))*t(2));
                        U4 = expm((R-1i*(Hfree))*t(4));

                        if t(8)==t(4)
                            U8 = U4;
                        else
                            U8 = expm((R-1i*(Hfree))*t(8));
                        end
                        U10 = expm((R-1i*(Hfree))*(t(2)+t(3)/2));
                    else
                        U5 = relaxation.Bterm(opt,expt,v_RF,Hfree_p,Iy,t(5),Ni_ENDOR,N_spinSys,R);
                        if t(5)==t(7)
                            U7=U5;
                        else
                            U7 = relaxation.Bterm(opt,expt,v_RF,Hfree_p,Iy,t(7),Ni_ENDOR,N_spinSys,R);
                        end

                        U1 = expm((R-1i*(Hnonsel))*t(1));

                        if t(1)==t(3)
                            U3 = U1;
                        else
                            U3 = expm((R-1i*(Hnonsel))*t(3));

                        end
                        
                        if t(1)==t(9)
                            U9 = U1;
                        elseif t(3)==t(9)
                            U9=U3;
                        else
                            U9 = expm((R-1i*(Hnonsel))*t(9));
                        end

                        U2 = expm((R-1i*(Hfree))*t(2));
                        U4 = expm((R-1i*(Hfree))*t(4));
                        if t(8)==t(4)
                            U8 = U4;
                        else
                            U8 = expm((R-1i*(Hfree))*t(8));
                        end
                        
                        U10 = expm((R-1i*(Hfree))*(t(2)+t(3)/2));
                    end
    
                    % Evolve the densitymatrix  
                    rho = relaxation.MatrixToLiouvilleBra(rho0)';

                    rho = U1*rho;
                    rho = U2*rho;           
                    rho = U3*rho; 
                    rho = U4*rho;
                    rho = U5*rho;          
                    rho_t = rho;    

    
                % loop over different separation of the RF pulses (x-axis)  
                for a = 1:Npts_EN
                    t6= t(6)+step_EN*(a-1);
                    U6 = expm((R-1i*(Hfree))*t6);

                    rho=rho_t;
                    rho = U6*rho;
                    rho = U7*rho;
                    rho = U8*rho;
                    rho = U9*rho;
                    rho = U9*rho;
                    rho = U10*rho;


                    value_Sy = 0;
                    for b=1
                       rho_f = relaxation.LiouvilleKetToMatrix(rho);
                       value_Sy = value_Sy+(real(trace(rho_f*Sy)));
                    end

                    if abs(HF_zz(1))>1/spinSys("T2e")*0.1
                        endor_amp_tmp(a) = endor_amp_tmp(a)+(value_Sy*S/(Nint*size(offsets,2)));  
                    end

                end
            end
            endor_amp = endor_amp + endor_amp_tmp;
        end

    end

end
end

function data_conv = lb(data,opt)
% convolutes the input data with a Lorentian and/or a
% Gaussian line broadening 
% CAUTION: this is a beta version and not fully tested
% convolution for time domain traces can easily be performed in Origin,
% etc. by multiplication with the corresponding exponential function
%
% input parameters: 
% data: data to be convoluted
% opt: options Map, should contain keys ('Lorentian'/'Gaussian' and
%       'lw_L'/'lw_G')
%
% output parameters:
% data_conv: convoluted data 
%
% February 2024 A. Kehl (akehl@gwdg.de)
%
data_n = (data-min(data))/(max(data)-min(data))-0.5;

if opt("Lorentzian") == 1
        dips("CAUTION: this is a beta version and not fully tested");

        
        Deltaend_L = opt('lw_L')*pi/0.1;
        % Lorentian
        endintens1 = (data_n(:));
        for i = 1:size(data,2)
            endintens(i) = endintens1(i)*exp(-Deltaend_L*i);   % Lorentzian line shape
        end
        endintens(1)=0.37*endintens(1);
        endamp_2d_L_conv(:) = real((endintens));
    
        if opt("Gaussian") == 1
            lw = opt('lw_G');
            Deltaend_G=(lw*pi/(0.1*sqrt(2*log(2))))^2;
            % Gaussian
            endintens1 = (endamp_2d_L_conv(:));
            for i = 1:size(data_n,2)
                endintens(i) = endintens1(i)*exp(-Deltaend_G*i^2/2);   % Gaussian line shape
            end
            endintens(1)=0.5*endintens(1);
            data_conv(:) = -abs((endintens));
            
        else 
            data_conv=endamp_2d_L_conv(:);
        end
    elseif opt("Gaussian")==1
        dips("CAUTION: this is a beta version and not fully tested");

        lw = opt('lw_G');
        Deltaend_G=(lw*pi/(0.1*sqrt(2*log(2))))^2;

        % Gaussian
        data=data+1000;
        endintens1 = (data_n(:));
        for i = 1:size(data_n,2)
            endintens(i) = endintens1(i)*exp(-Deltaend_G*i^2/2);   % Gaussian line shape
        end
        endintens(1)=0.5*endintens(1);
        data_conv(:) = -abs((endintens));
        
    else 
        data_conv=data;
end
end