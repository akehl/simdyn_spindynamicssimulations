% exemplary setup of a 19F Mims ENDOR simulation at 34 GHz
% 
% March 2024 A. Kehl  (akehl@gwdg.de)

%% define constants 
constants = define_constants();

%% set up of the spin system
T=0.066;                                    % T_dip in MHz
% obligatory: create the spin system
spinSys = defineSpinSys.create( ...         
        1/2, ...                            % S 
        [2.00886,2.00610,2.00211], ...      % g-tensor
        1, ...                              % one spin system
        3, ...                              % with 3 nuclei
        {'19F','1H','1H'}, ...              % type of nuclei
        [[2*T,-T,-T],[0,0,0],[0,0,0]], ...  % HF coupling (e to nuc.)
        [[161,1,0],[0,0,0],[0,0,0]]);       % Euler angles of HF coupling
% number of spin systems should be 1 or equivalent to number of nuclei

    
% then: add values for optional effects, thereby the order is arbitrary

% optional: define 14N as nucleus for only the EPR calculation
spinSys = defineSpinSys.setEPRNucs( ...     % update the spin system
        constants, ...                      % using the constant
        spinSys, ...                        % and the prev. created system
        {'14N'}, ...                        % type of nuclei
        [[15,11,95.8]], ...                 % HF coupling
        [[0,0,0]], ...                      % Euler angles of HF coupling
        [[1.2,0.54,-1.7]], ...              % if applicable: NQ coupling
        [[0,0,0]]);                         % Euler angles of NQ coupling
    
% optional: define relaxation times, 
%           required if relaxation effects are used
%           set values to Inf to neglect them
spinSys = relaxation.defineRelax( ...       % update the spin system
        spinSys, ...                        % using the prev. created sys.
        4E-3, ...                           % T1e in s
        Inf, ...                            % T1n in s
        5E-6, ...                           % T2e in s
        3E-3, ...                           % T2n in s
        Inf);                               % T2dzq in s
    
% optional: define ND coupling
D = 6.5;                                    % D_dip in kHz
spinSys = defineSpinSys.setD( ...           % update the spin system
        spinSys, ...                        % using the prev. created sys.
        [[2*D,-D,-D],[2*D,-D,-D]], ...      % ND coupling in kHz
        [[-16.9,143.0,0],[-47.9,34.3,0]]);  % Euler angles of ND coupling
    
% optional: define CS values
spinSys = defineSpinSys.setCS( ...          % update the spin system
        spinSys, ...                        % using the prev. created sys.
        [[205,310,358],[0,0,0],[0,0,0]],... % CS values in ppm
        [[29,14,-87],[0,0,0],[0,0,0]]);     % Euler angles for CS

%% define the experimental parameters
% obligatory: create the experiment first
expt = defineExp.create( ...                
        33.7727, ...                        % nu MW in GHz
        12052.1, ...                        % B0 in G
        0.5, ...                            % EPR setp size in G
        0.001, ...                          % ENDOR step size in MHz
        0.3, ...                            % ENDOR sweep width in MHz
        [12,2000,12,1000,200000,1000,12]);  % time steps in ns
% time steps refer to pulse length from beginning to end of a pulse and
% delay times from pulse end to next pulse beginning
% order of pulses dependent on sequence    

% obligatory: then calculate pulse strength according to experiment
expt = Mims.setFieldStrength( ...           % update the experiment
        constants, ...                      % using the constants
        expt, ...                           % and the prev. created exp.
        true, ...                           % fix pulse strength? Y/N
        [0,0]);                             % if N: set values
% the last array is ignored if pulse strengths are fixed automatically in
% dependence of the pulse lengths

% optional: define freq. domain EPR calculation
expt = defineExp.freq( ...                  % update the experiment
        expt, ...                           % using the prev. created exp.
        32, ...                             % start of nu MW in GHz
        3, ...                              % sweep width for EPR in GHz
        0.001);                             % resolution for EPR in GHz

% optional: define turning angle of RF pulse
expt = defineExp.setAng( ...                % update the experiment
        expt, ...                           % using the prev. created exp.
        163);                               % turning angle in degree
    
% optional: define input file for MW pulse shape
expt = defineExp.pulseInput( ...            % update the experiment
        expt, ...                           % using the prev. created exp.
 '231016_01d_MLR09_50K_pulseShape_12ns',... % data file of pulse shape
        false);                             % consider only 1 pi/2 pulse
    
%% set options
opt = setOpt( ...
        spinSys, ...                        % using the spin sys
        containers.Map( ...                 % create options Map
             ...                            % first the key words:
            ['freqDomain', ...              % freq. dom. EPR: Y/N
            'powder', ...                   % powder: Y/N
            'Nang', ...                     % Nang for powder: integer
            'Relax', ...                    % relaxation: Y/N            
            'Bterm'],...                    % pseudo-first order a.: Y/N
            ...                             % then the resp. values
            [1, ...                         % freq. dom. EPR: Y
            1, ...                          % powder: Y
            50, ...                         % Nang = 50
            0, ...                          % relaxation: N
            0]));                           % pseudo-first order a.: N
% for further optional parameters, increas number of Map elements
        
%% get spin operators
spinOps = spin_operators( ...       
            spinSys("S"), ...               % S as defined above
            spinSys("I"), ...               % I for defined nuclei (array)
            spinSys("Ni_ENDOR"), ...        % number of nuclei (n)
            spinSys("N_SpinSys"));          % number of spin systems
                                            % should be 1 or n
        
% This function can also be called independently from an ENDOR simulation 
% to generate a spin operator set. Then the values have to be set manually.
        
%% actual ENDOR calculation
[ ...                                       % output array
    endor_amp, ...                       % ENDOR intensities
    endor_amp_conv, ...                  % convoluted ENDOR intensities
    x_coords, ...                        % x axis coordinates
    v_L] ...                                % Larmor frequencies
     = Mims.ENDOR( ...                      % function call: Mims ENDOR
            constants, ...                  % using the constants
            spinSys, ...                    % using the spin system
            spinOps, ...                    % using the spin operators
            expt, ...                       % using the experiment
            opt);                           % using the options

%% optional: plotting
sim = endor_amp(:)-endor_amp(1);            % base line correction
sim = sim/min(sim);                         % normalization

plot((x_coords(:))*1e-6,sim)                % plot: I(ENDOR) vs. nu(RF)