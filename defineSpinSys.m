classdef defineSpinSys
    % functions to define the spinsystem
    % 
    % February 2024 A. Kehl (akehl@gwdg.de)

methods (Static=true)
    
    function SpinSys = create(S,g,N_SpinSys,N_Nuclei,Nuclei,A_ten,A_ang,Q_ten,Q_ang)
    % creates the Map defining the spinsystem
    % is directly called for the set up
    % 
    % input parameters:
    % S: electron spin quantum number
    % g: g tensor (diagonal values)
    % N_SpinSys: number of individual spin systems
    % N_Nuclei: number of nuclei
    % Nuclei: nuclei in the spin system (e.g. '1H')
    % A_ten: diagonal values of A tensor
    % A_ang: Euler angles for A tensor (alpha, beta, gamma)
    % Q_ten: diagonal values of Q tensor
    % Q_ang: Euler angles for Q tensor (alpha, beta, gamma)
    % 
    % output parameters:
    % SpinSys: the Map containing the spinsystem parameters
    %
    % February 2024 A. Kehl (akehl@gwdg.de)

    arguments
        S
        g
        N_SpinSys
        N_Nuclei
        Nuclei
        A_ten
        A_ang
        Q_ten=[]
        Q_ang=[]
    end

        SpinSys = containers.Map;
    
        SpinSys("S") = S;
        SpinSys("g") = diag(g);
        SpinSys("g_iso") = (g(1)+g(2)+g(3))/3;      
        SpinSys("Ni_ENDOR") = N_Nuclei;
        SpinSys("Nuclei") = Nuclei;
        SpinSys("N_SpinSys")=N_SpinSys;
    
        I=zeros(N_Nuclei,1);

        for i=1:N_Nuclei
            if ismember(Nuclei{i}, ['1H','19F'] ) 
                I(i)=1/2;
            elseif ismember(Nuclei{i},['2D','14N'])
                I(i)=1;
            elseif ismember(Nuclei{i}, ['17O']) 
                I(i)=5/2;
            else
                print("Nucleus ",i, " not found")
            end
        end
        SpinSys("I") = I;
    
        L = zeros(N_Nuclei,12);
    
        for i=1:N_Nuclei
            L(i,1:3) = A_ten(3*i-2:3*i)*10^6;
            L(i,4:6)= A_ang(3*i-2:3*i);
            if size(Q_ten,2)>0
                SpinSys("Q_used")=true;
                L(i,7:9)= Q_ten(3*i-2:3*i)*10^6;
                L(i,10:12)= Q_ang(3*i-2:3*i);
            else
                SpinSys("Q_used")=false;
            end
        end
      

        [A,Q] = to_g_frame(L,N_Nuclei);    % transform L to g-frame A and Q tensors
        SpinSys("A") = A;
        SpinSys("Q")= Q;
        SpinSys("EPR_Nucs_used")=false;
        SpinSys("CS_used")=false;
        SpinSys("D_used")=false;
    end

    function SpinSys_out = setCS(spinSys_in,CS_ten,CS_ang)
        % adds chemical shielding values to the spinsystem
        % is directly called for the set up
        %
        % input parameters:
        % spinSys_in: the Map with the spin parameters
        % CS_ten: the diagonal values of the shielding tensor in ppm
        % CS_ang: the Euler angles for the shielding tensor (alpha, beta, gamma)
        %
        % output parameters:
        % SpinSys_out: updated Map with spin parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)

        SpinSys_out = spinSys_in;
        Ni_ENDOR = spinSys_in("Ni_ENDOR");
        SpinSys_out('CS_used')=true;

        L = zeros(Ni_ENDOR,12);
        for i=1:Ni_ENDOR
                L(i,1:3) = CS_ten(3*i-2:3*i)*10^6;
                L(i,4:6)= CS_ang(3*i-2:3*i);
        end
        % disp(L)

        [CS,~] = to_g_frame(L,Ni_ENDOR);

        SpinSys_out("CS")=CS;
        % disp(CS)
    end
    
    function SpinSys_out = setD(spinSys_in,D_ten,D_ang)
        % adds nuclear dipolar coupling values to the spinsystem
        % is directly called for the set up
        %
        % input parameters:
        % spinSys_in: the Map with the spin parameters
        % D_ten: the diagonal values of the coupling tensor in MHz
        % D_ang: the Euler angles for the coupling tensor (alpha, beta, gamma)
        %
        % output parameters:
        % SpinSys_out: updated Map with spin parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)

        SpinSys_out = spinSys_in;
        Ni_Dip = size(D_ten,2)/3;
        SpinSys_out('D_used')=true;

        L = zeros(Ni_Dip,12);
        for i=1:Ni_Dip
                L(i,1:3) = D_ten(3*i-2:3*i)*10^3;
                L(i,4:6)= D_ang(3*i-2:3*i);
        end
        % disp(L)

        [D,~] = to_g_frame(L,Ni_Dip);

        SpinSys_out("D")=D;
        % disp(CS)
    end

    function SpinSys_out = setEPRNucs(constants,spinSys_in,EPR_Nuclei,A_ten_EPR,A_ang_EPR,Q_ten_EPR,Q_ang_EPR)
        % adds nuclei to be considered only for the EPR calculation to the spinsystem
        % is directly called for the set up
        %
        % input parameters:
        % constants: the Map with the constants
        % spinSys_in: the Map with the spin parameters
        % EPR_Nuclei: the nuclei for the EPR calculation (e.g. '14N')
        % A_ten_EPR: the diagonal values of the hf tensor in MHz
        % A_ang_EPR: the Euler angles for the hf tensor (alpha, beta, gamma)
        % Q_ten_EPR: the diagonal values of the nq tensor in MHz
        % Q_ang_EPR: the Euler angles for the nq tensor (alpha, beta, gamma)
        %
        % output parameters:
        % SpinSys_out: updated Map with spin parameters
        %
        % February 2024 A. Kehl (akehl@gwdg.de)
        arguments
            constants
            spinSys_in
            EPR_Nuclei
            A_ten_EPR
            A_ang_EPR
            Q_ten_EPR=[]
            Q_ang_EPR=[]
        end

        SpinSys_out = spinSys_in;
        
        SpinSys_out("EPR_Nuclei") = EPR_Nuclei;       
        Ni_EPR = size(EPR_Nuclei,2);
        SpinSys_out("Ni_EPR") = Ni_EPR;

        I=zeros(Ni_EPR,1);
        for i=1:Ni_EPR
            if ismember(EPR_Nuclei{i}, ['1H','19F'] ) 
                I(i)=1/2;
            elseif ismember(EPR_Nuclei{i},['2D','14N'])
                I(i)=1;
            elseif ismember(EPR_Nuclei{i}, ['17O']) 
                I(i)=5/2;
            else
                print("EPR Nucleus ",i, " not found")
            end
        end
        SpinSys_out("I_EPR") = I;

        g_N_EPR = zeros(Ni_EPR,1);
        for i=1:size(Ni_EPR,2)
            if strcmp(EPR_Nuclei(i),"1H")           
                g_N_EPR(i) = constants("GN_1H");                                 
            elseif strcmp(EPR_Nuclei(i),"2D")    
                g_N_EPR(i) = constants("GN_2D");                     
            elseif strcmp(EPR_Nuclei(i),"14N")    
                g_N_EPR(i) = constants("GN_14N");                    
            elseif strcmp(EPR_Nuclei(i),"17O")    
                g_N_EPR(i) = constants("GN_17O");                    
            elseif strcmp(EPR_Nuclei(i),"19F")    
                g_N_EPR(i) = constants("GN_19F");                   
            end
        end
        SpinSys_out("g_N_EPR") = g_N_EPR;

        SpinSys_out("EPR_Nucs_used")=true;
        SpinSys_out("EPR_Q_used")=false;

        L_EPR = zeros(Ni_EPR,12);
        for i=1:Ni_EPR
            L_EPR(i,1:3) = A_ten_EPR(3*i-2:3*i)*10^6;
            L_EPR(i,4:6)= A_ang_EPR(3*i-2:3*i);
            if size(Q_ten_EPR,2)>0
                SpinSys_out("EPR_Q_used")=true;
                L_EPR(i,7:9)= Q_ten_EPR(3*i-2:3*i)*10^6;
                L_EPR(i,10:12)= Q_ang_EPR(3*i-2:3*i);
            end
        end
        [A_EPR,Q_EPR] = to_g_frame(L_EPR,Ni_EPR);

        SpinSys_out("A_EPR") = A_EPR;
        SpinSys_out("Q_EPR") = Q_EPR;
    end
  
end
end